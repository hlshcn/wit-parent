/*
 Navicat Premium Data Transfer

 Source Server         : 1806
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : wisdom_medcial

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 07/01/2021 09:12:27
*/

create database wit-system

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_department
-- ----------------------------
DROP TABLE IF EXISTS `tb_department`;
CREATE TABLE `tb_department`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '科室id',
  `hospital_id` bigint(20) NULL DEFAULT NULL COMMENT '医院id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '科室名称',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `physician_num` bigint(20) NULL DEFAULT NULL COMMENT '科室医师人数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_department
-- ----------------------------

-- ----------------------------
-- Table structure for tb_department_physician_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_department_physician_category`;
CREATE TABLE `tb_department_physician_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '中间表id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '级别',
  `departments_id` bigint(20) NULL DEFAULT NULL COMMENT '科室id',
  `physician_id` bigint(20) NULL DEFAULT NULL COMMENT '医生id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室医生中间表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_department_physician_category
-- ----------------------------

-- ----------------------------
-- Table structure for tb_hospital
-- ----------------------------
DROP TABLE IF EXISTS `tb_hospital`;
CREATE TABLE `tb_hospital`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院名',
  `physician_num` bigint(20) NULL DEFAULT NULL COMMENT '医师数量',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医院表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_hospital
-- ----------------------------

-- ----------------------------
-- Table structure for tb_patient
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient`;
CREATE TABLE `tb_patient`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '患者id',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者手机号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `birthday` datetime NULL DEFAULT NULL COMMENT '出生日期',
  `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付总额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_patient
-- ----------------------------

-- ----------------------------
-- Table structure for tb_patient_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient_record`;
CREATE TABLE `tb_patient_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '患者问诊记录id',
  `createtime` datetime NULL DEFAULT NULL COMMENT '问诊时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付问诊金额',
  `condition` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病情',
  `physician_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生姓名',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生职称',
  `hospital_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生隶属医院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者问诊记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_patient_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_physician
-- ----------------------------
DROP TABLE IF EXISTS `tb_physician`;
CREATE TABLE `tb_physician`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '医师ID',
  `department_physician_category_id` bigint(20) NULL DEFAULT NULL COMMENT '科室医生中间表ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师名',
  `years` int(11) NULL DEFAULT NULL COMMENT '年限',
  `state` int(11) NULL DEFAULT NULL COMMENT '审核状态(0未审核1通过2未通过)',
  `doctor_qualification_certificate` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师资格证',
  `physician_practicing_certificate` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师执业证书',
  `id_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `total_revenue` decimal(20, 2) NULL DEFAULT NULL COMMENT '总收入金额',
  `diagnosis_times` bigint(11) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医师表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_physician
-- ----------------------------

-- ----------------------------
-- Table structure for tb_physician_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_physician_record`;
CREATE TABLE `tb_physician_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '医生问诊记录id',
  `createtime` datetime NULL DEFAULT NULL COMMENT '问诊时间',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '收取问诊金额',
  `condition` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病情',
  `physician_id` bigint(20) NULL DEFAULT NULL COMMENT '医师ID',
  `physician_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生姓名',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生等级',
  `hospital_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生隶属医院',
  `patient_id` bigint(20) NULL DEFAULT NULL COMMENT '患者ID',
  `patient_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医生问诊记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_physician_record
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
