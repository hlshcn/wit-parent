/*
 Navicat Premium Data Transfer

 Source Server         : 10.4.157.30
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 10.4.157.30:3306
 Source Schema         : wit-system

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 09/01/2021 16:45:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_department
-- ----------------------------
DROP TABLE IF EXISTS `tb_department`;
CREATE TABLE `tb_department`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '科室id',
  `hospital_id` bigint(20) NULL DEFAULT NULL COMMENT '医院id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '科室名称',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `physician_num` bigint(20) NULL DEFAULT NULL COMMENT '科室医师人数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_department
-- ----------------------------
INSERT INTO `tb_department` VALUES (1, 1, '眼科', 0, 0, 1, 1.00);
INSERT INTO `tb_department` VALUES (2, 1, '耳科', 0, 0, 1, 1.00);
INSERT INTO `tb_department` VALUES (3, 1, '骨科', 0, 0, 1, 1.00);
INSERT INTO `tb_department` VALUES (4, 2, '眼科', 0, 0, 1, 1.00);
INSERT INTO `tb_department` VALUES (5, 2, '耳科', 0, 0, 1, 1.00);
INSERT INTO `tb_department` VALUES (6, 2, '骨科', 0, 0, 1, 1.00);

-- ----------------------------
-- Table structure for tb_department_physician_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_department_physician_category`;
CREATE TABLE `tb_department_physician_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '中间表id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '级别',
  `departments_id` bigint(20) NULL DEFAULT NULL COMMENT '科室id',
  `physician_id` bigint(20) NULL DEFAULT NULL COMMENT '医生id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室医生中间表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_department_physician_category
-- ----------------------------
INSERT INTO `tb_department_physician_category` VALUES (1, '主任医师', 1, 1);
INSERT INTO `tb_department_physician_category` VALUES (2, '专家医师', 1, 2);
INSERT INTO `tb_department_physician_category` VALUES (3, '普通医师', 1, 3);
INSERT INTO `tb_department_physician_category` VALUES (4, '主任医师', 2, 4);
INSERT INTO `tb_department_physician_category` VALUES (5, '专家医师', 2, 5);
INSERT INTO `tb_department_physician_category` VALUES (6, '普通医师', 2, 6);
INSERT INTO `tb_department_physician_category` VALUES (7, '主任医师', 3, 7);
INSERT INTO `tb_department_physician_category` VALUES (8, '专家医师', 3, 8);
INSERT INTO `tb_department_physician_category` VALUES (9, '普通医师', 3, 9);
INSERT INTO `tb_department_physician_category` VALUES (10, '主任医师', 4, 10);
INSERT INTO `tb_department_physician_category` VALUES (11, '专家医师', 4, 11);
INSERT INTO `tb_department_physician_category` VALUES (12, '普通医师', 4, 2);
INSERT INTO `tb_department_physician_category` VALUES (13, '主任医师', 5, 12);
INSERT INTO `tb_department_physician_category` VALUES (14, '专家医师', 5, 13);
INSERT INTO `tb_department_physician_category` VALUES (15, '普通医师', 5, 14);
INSERT INTO `tb_department_physician_category` VALUES (16, '主任医师', 6, 15);
INSERT INTO `tb_department_physician_category` VALUES (17, '专家医师', 6, 16);
INSERT INTO `tb_department_physician_category` VALUES (18, '普通医师', 6, 17);

-- ----------------------------
-- Table structure for tb_hospital
-- ----------------------------
DROP TABLE IF EXISTS `tb_hospital`;
CREATE TABLE `tb_hospital`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院名',
  `physician_num` bigint(20) NULL DEFAULT NULL COMMENT '医师数量',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医院表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_hospital
-- ----------------------------
INSERT INTO `tb_hospital` VALUES (1, '中国医学科学院北京协和医院', 0, 0, 0, 0.00);
INSERT INTO `tb_hospital` VALUES (2, '复旦大学附属中山医院', 0, 0, 0, 0.00);

-- ----------------------------
-- Table structure for tb_patient
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient`;
CREATE TABLE `tb_patient`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '患者id',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者手机号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `diagnosis_times` bigint(20) NULL DEFAULT NULL COMMENT '问诊次数',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '出生日期',
  `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付总额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_patient
-- ----------------------------
INSERT INTO `tb_patient` VALUES (1, '1399854652', '张三', 1, '2021-01-05 08:06:07', 158.00);
INSERT INTO `tb_patient` VALUES (2, '1512452322', '李四', 1, '2021-01-05 08:06:52', 258.00);
INSERT INTO `tb_patient` VALUES (3, '14123568796', '王五', 1, '2021-01-27 08:06:55', 145.00);
INSERT INTO `tb_patient` VALUES (4, '123032145689', '赵六', 1, '2021-01-22 08:06:58', 144.00);

-- ----------------------------
-- Table structure for tb_patient_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_patient_record`;
CREATE TABLE `tb_patient_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '患者问诊记录id',
  `createtime` datetime(0) NULL DEFAULT NULL COMMENT '问诊时间',
  `patient_id` bigint(20) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '支付问诊金额',
  `patient_condition` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病情',
  `physician_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生姓名',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生职称',
  `hospital_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问诊医生隶属医院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者问诊记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_patient_record
-- ----------------------------
INSERT INTO `tb_patient_record` VALUES (1, '2021-01-13 08:51:08', 1, '张三', 18, 100.00, '发烧', '扁鹊', '主任医师', '中国医学科学院北京协和医院');
INSERT INTO `tb_patient_record` VALUES (2, '2021-01-06 10:16:29', 2, '李四', 20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_patient_record` VALUES (3, '2021-01-13 10:16:31', 3, '王五', 23, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_patient_record` VALUES (4, '2021-01-20 10:16:33', 4, '赵六', 17, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_physician
-- ----------------------------
DROP TABLE IF EXISTS `tb_physician`;
CREATE TABLE `tb_physician`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '医师ID',
  `department_physician_category_id` bigint(20) NULL DEFAULT NULL COMMENT '科室医生中间表ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师名',
  `years` int(11) NULL DEFAULT NULL COMMENT '年限',
  `state` int(11) NULL DEFAULT NULL COMMENT '审核状态(0未审核1通过2未通过)',
  `doctor_qualification_certificate` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师资格证',
  `physician_practicing_certificate` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医师执业证书',
  `id_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `total_revenue` decimal(20, 2) NULL DEFAULT NULL COMMENT '总收入金额',
  `diagnosis_times` bigint(11) NULL DEFAULT NULL COMMENT '问诊次数',
  `comment_num` bigint(20) NULL DEFAULT NULL COMMENT '评价次数',
  `score` double(20, 2) NULL DEFAULT NULL COMMENT '总分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医师表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_physician
-- ----------------------------
INSERT INTO `tb_physician` VALUES (1, 1, '扁鹊', 200, 1, NULL, NULL, '140524100001010000', 0.00, 3, 2, 1.00);
INSERT INTO `tb_physician` VALUES (2, 4, '华佗', 300, 1, NULL, NULL, '140524100001010000', 0.00, 8, 1, 1.00);
INSERT INTO `tb_physician` VALUES (3, 7, '张仲景', 321, 1, NULL, NULL, '140524100001010000', 0.00, 20, 11, 1.00);
INSERT INTO `tb_physician` VALUES (4, 8, '皇甫谧', 123, 1, NULL, NULL, '140524100001010000', 0.00, 3, 1, 1.00);
INSERT INTO `tb_physician` VALUES (5, 9, '叶桂', 351, 1, NULL, NULL, '140524100001010000', 0.00, 13, 12, 1.00);
INSERT INTO `tb_physician` VALUES (6, 10, '孙思邈', 125, 1, NULL, NULL, '140524100001010000', 0.00, 2, 1, 1.00);
INSERT INTO `tb_physician` VALUES (7, 13, '薛生白', 543, 1, NULL, NULL, '140524100001010000', 0.00, 2, 1, 1.00);
INSERT INTO `tb_physician` VALUES (8, 16, '宋慈', 234, 1, NULL, NULL, '140524100001010000', 0.00, 2, 1, 1.00);
INSERT INTO `tb_physician` VALUES (9, 17, '李时珍', 512, 1, NULL, NULL, '140524100001010000', 0.00, 2, 1, 1.00);
INSERT INTO `tb_physician` VALUES (10, 18, '葛洪', 123, 1, NULL, NULL, '140524100001010000', 0.00, 22, 11, 1.00);

-- ----------------------------
-- Table structure for tb_physician_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_physician_record`;
CREATE TABLE `tb_physician_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '医生问诊记录id',
  `createtime` datetime(0) NULL DEFAULT NULL COMMENT '问诊时间',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '收取问诊金额',
  `physician_condition` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病情',
  `physician_id` bigint(20) NULL DEFAULT NULL COMMENT '医师ID',
  `physician_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生姓名',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生等级',
  `hospital_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医生隶属医院',
  `patient_id` bigint(20) NULL DEFAULT NULL COMMENT '患者ID',
  `patient_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医生问诊记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_physician_record
-- ----------------------------
INSERT INTO `tb_physician_record` VALUES (1, '2021-01-05 08:34:09', 100.00, '发烧', 1, '扁鹊', '主任医师', '中国医学科学院北京协和医院', 1, '张三', 18);

SET FOREIGN_KEY_CHECKS = 1;
