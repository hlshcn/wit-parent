package com.qianxi.wit.payment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/alipay")
public class WebController {
    /**
     * 返回returnUrl页面
     * @return
     */
    @GetMapping("/returnUrl")
    public String returnUrl(){
        return "returnUrl";
    }

    @GetMapping("/pay")
    public String pay(){
        return "pay";
    }
}
