package com.qianxi.wit.payment.controller;

import com.qianxi.common.utils.CodeUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.common.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 发送短信服务
 */
@RestController
public class SendSmsController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/sendSms")
    public Result sendSms(String phone) {
        //1.生成验证码
        String code = CodeUtils.generateRandomCode(6);
        //2.将验证码发送至手机号
        SendSmsUtil.sendSms(phone, code);
        //3.将验证码存入redis，5分钟过期
        redisTemplate.opsForValue().set("phone:" + phone, code, 5, TimeUnit.MINUTES);
        return Result.ok();
    }
}
