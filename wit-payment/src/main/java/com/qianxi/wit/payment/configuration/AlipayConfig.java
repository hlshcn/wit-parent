package com.qianxi.wit.payment.configuration;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号,开发时使用沙箱提供的APPID，生产环境改成自己的APPID
    public static String APP_ID = "2021000116695722"; //测试

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCVwI/s2opbnc2Pg1cdGXpH5JeCdbpAhWo//MWrvSAvzFxcCsmVDC42HehjO4/va6azvi1yq+at+qfjQQZ5RAxqUzJuhhdosaZrnQnIdvifQxkYqV+f/uudqyL1dLY+w15ZP1K02Q6+ZzighsQrmCvo4ip9fjm0vsC0VYVQuMx/sofa5zwmDTqU+tk9mJwt023xLBGzit9mUSZEKny6PKmJdqG9s3T+fRyyTKJBIsFW55n2zee2m/QhaR5+K9D7iX/AcI1pxvhU9nTfle8Bgsu6iHC2Yx0OwQyrbje15quY92Rc7A/q0mMHoZH1+3hMjpJLqOAQDCorP5imlAZ0tp2HAgMBAAECggEAflg51e7IPuXTsdpxHBcLyS4++g3Tt24Yp6XfLLdjOZCk9+8OBleBgZ0YHOj61lgwDIROmeoYtP6+uQh6qsNT6j0JVqCBjY4cx7x1rZFa0BvvR5RXHokYRy1FUGxI+okgyBGZ+QUM4G38I8uwJMu9eD2g9cISc8FgmoVw9hsERlOITS9VSJfS1Zog1niIXXMgvKGBWHo37NGM2fNuHeSgfqimwTyqTCx8fbwdyPAcm33SQYZLmWBmztEV6hhbXQ6S1E3h6RGQhRGFORABwOZ9ZUMZf0kXKF6Vcs2I7vVusV+X5s0P73Zz6zdqDKy59wE71kRNRnITRu0oNZI7yJ8U8QKBgQD7070DwKT0oQSlqVz+IqFLB9oa3OUtIJ83pO7AhXTY++Of79E8P6E5MH0svKvrthHsv8jDEvFgoz49FryJGqPEX1meZXukaUkH7repupioqi1Kxr7ZKRG8T/U2bzKKojcBrKF5i1gAcRaFU0VpmkTfOVV2xb2ReKVal3cNFzi27wKBgQCYO9FPfrRZd0ipiU8su2u0FRqU6XSyTMM+qTuKWHIYYmGkLxYJbJ1fevlPjooPUrLl64u84W3NsH6rLsOBZMLlsmouu8F2DJYhmvueyqNhuwB8qnbBjf7+t4KZbPj3qo5lOVczThKSeMkOXeOW3fqp3XTiAG5XGMzs0ww1J/nC6QKBgQCHRaIO1fjF/4iDhIqTaP7DUyBxtZ7VsuRT+6DUgdvWgtgvHZ+DgZqVIdrboS2onFDcn9i0EQTgKFBSeFgmPc0iuyVk1JaqJnQ4cp7t6XH/iAp0VHZIKKuCQGE0MF8x4RkzKQxEQfmvow+55pUuuNOuJRonhujhr7EDDMHComqwuQKBgCizhYjNhYaN/91kGskI6wjgw5SgvlSVdXYhZQveDdM7KoPtGuU2JvVKObHtmDf0kmQqj9fWJbdiwtsdy6OivwEOrJkGFflr8A/YEgIoh3ovtzedk9a4Ej8j7UT7/6yn6x5hMFFFRG3o4Uh4DcteWhBkZ0Wte9LJjLCc1Jy5rWtRAoGBAMlCDT28CH8fq4oYruXpTvqFbh+NP82PNZNVBagVo2IPp5BX1oRpDgf11OwF8RSLuhokj8VXE5esXD8xbzx9UixxoZw87dsZ9KQEeg4zcM5G9FA0oHIQ90/oLXp3Zv3XXf7mIfDzKh1ExtS0l59FFFEmpUPK5aXMuadZnsH31Zx3";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlcCP7NqKW53Nj4NXHRl6R+SXgnW6QIVqP/zFq70gL8xcXArJlQwuNh3oYzuP72ums74tcqvmrfqn40EGeUQMalMyboYXaLGma50JyHb4n0MZGKlfn/7rnasi9XS2PsNeWT9StNkOvmc4oIbEK5gr6OIqfX45tL7AtFWFULjMf7KH2uc8Jg06lPrZPZicLdNt8SwRs4rfZlEmRCp8ujypiXahvbN0/n0cskyiQSLBVueZ9s3ntpv0IWkefivQ+4l/wHCNacb4VPZ035XvAYLLuohwtmMdDsEMq243tearmPdkXOwP6tJjB6GR9ft4TI6SS6jgEAwqKz+YppQGdLadhwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost/alipay/notifyUrl";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://localhost/alipay/returnUrl";
    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "UTF-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do"; //测试

    // 支付宝网关
    public static String log_path = "E:\\";

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis() + ".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
