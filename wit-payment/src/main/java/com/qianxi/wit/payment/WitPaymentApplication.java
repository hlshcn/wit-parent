package com.qianxi.wit.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class WitPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(WitPaymentApplication.class, args);
    }

}
