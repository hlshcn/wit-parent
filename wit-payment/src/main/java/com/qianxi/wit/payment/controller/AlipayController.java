package com.qianxi.wit.payment.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.qianxi.common.utils.IdWorker;
import com.qianxi.wit.payment.configuration.AlipayConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AlipayController {
    @Autowired
    private AlipayClient alipayClient;
    @Autowired
    private IdWorker idWorker;

    /**
     * 结算页面
     */
    @RequestMapping("/payPage")
    public String payPage(
            String totalAmount,
            String subject,
            String body,
            String outTradeNo
    ) throws AlipayApiException {
        //0.准备对象
        //1.
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
        //2.基本信息
//        String out_trade_no = idWorker.nextId() + "";
        String out_trade_no = outTradeNo;
        //付款金额，必填  ShopName
        String total_amount = totalAmount;
        //订单名称，必填
//        String subject = subject;
        //商品描述，可空
//        String body = map.get("body").toString();
        //3.设置请求参数
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //4.返回信息
        String url = alipayClient.pageExecute(alipayRequest).getBody();
        return url;
    }
}
