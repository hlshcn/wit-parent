package com.qianxi.wit.payment;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.factory.Factory.Payment;
import com.alipay.easysdk.kernel.Config;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePrecreateResponse;

public class Main {
    public static void main(String[] args) throws Exception {
        // 1. 设置参数（全局只需设置一次）
        Factory.setOptions(getOptions());
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            AlipayTradePrecreateResponse response = Payment.FaceToFace()
                    // 调用optional扩展方法，完成可选业务参数（biz_content下的可选字段）的设置
                    .preCreate("Apple iPhone11 128G", "20150320010101001", "1.00");
            // 3. 处理响应或异常
            if (ResponseChecker.success(response)) {
                System.out.println("调用成功");
            } else {
                System.err.println("调用失败，原因：" + response.msg + "，" + response.subMsg);
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static Config getOptions() {
        Config config = new Config();
        config.protocol = "https";
        config.gatewayHost = "openapi.alipaydev.com/gateway.do";
        config.signType = "RSA2";

        config.appId = "2021000116695722";

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        config.merchantPrivateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCVwI/s2opbnc2Pg1cdGXpH5JeCdbpAhWo//MWrvSAvzFxcCsmVDC42HehjO4/va6azvi1yq+at+qfjQQZ5RAxqUzJuhhdosaZrnQnIdvifQxkYqV+f/uudqyL1dLY+w15ZP1K02Q6+ZzighsQrmCvo4ip9fjm0vsC0VYVQuMx/sofa5zwmDTqU+tk9mJwt023xLBGzit9mUSZEKny6PKmJdqG9s3T+fRyyTKJBIsFW55n2zee2m/QhaR5+K9D7iX/AcI1pxvhU9nTfle8Bgsu6iHC2Yx0OwQyrbje15quY92Rc7A/q0mMHoZH1+3hMjpJLqOAQDCorP5imlAZ0tp2HAgMBAAECggEAflg51e7IPuXTsdpxHBcLyS4++g3Tt24Yp6XfLLdjOZCk9+8OBleBgZ0YHOj61lgwDIROmeoYtP6+uQh6qsNT6j0JVqCBjY4cx7x1rZFa0BvvR5RXHokYRy1FUGxI+okgyBGZ+QUM4G38I8uwJMu9eD2g9cISc8FgmoVw9hsERlOITS9VSJfS1Zog1niIXXMgvKGBWHo37NGM2fNuHeSgfqimwTyqTCx8fbwdyPAcm33SQYZLmWBmztEV6hhbXQ6S1E3h6RGQhRGFORABwOZ9ZUMZf0kXKF6Vcs2I7vVusV+X5s0P73Zz6zdqDKy59wE71kRNRnITRu0oNZI7yJ8U8QKBgQD7070DwKT0oQSlqVz+IqFLB9oa3OUtIJ83pO7AhXTY++Of79E8P6E5MH0svKvrthHsv8jDEvFgoz49FryJGqPEX1meZXukaUkH7repupioqi1Kxr7ZKRG8T/U2bzKKojcBrKF5i1gAcRaFU0VpmkTfOVV2xb2ReKVal3cNFzi27wKBgQCYO9FPfrRZd0ipiU8su2u0FRqU6XSyTMM+qTuKWHIYYmGkLxYJbJ1fevlPjooPUrLl64u84W3NsH6rLsOBZMLlsmouu8F2DJYhmvueyqNhuwB8qnbBjf7+t4KZbPj3qo5lOVczThKSeMkOXeOW3fqp3XTiAG5XGMzs0ww1J/nC6QKBgQCHRaIO1fjF/4iDhIqTaP7DUyBxtZ7VsuRT+6DUgdvWgtgvHZ+DgZqVIdrboS2onFDcn9i0EQTgKFBSeFgmPc0iuyVk1JaqJnQ4cp7t6XH/iAp0VHZIKKuCQGE0MF8x4RkzKQxEQfmvow+55pUuuNOuJRonhujhr7EDDMHComqwuQKBgCizhYjNhYaN/91kGskI6wjgw5SgvlSVdXYhZQveDdM7KoPtGuU2JvVKObHtmDf0kmQqj9fWJbdiwtsdy6OivwEOrJkGFflr8A/YEgIoh3ovtzedk9a4Ej8j7UT7/6yn6x5hMFFFRG3o4Uh4DcteWhBkZ0Wte9LJjLCc1Jy5rWtRAoGBAMlCDT28CH8fq4oYruXpTvqFbh+NP82PNZNVBagVo2IPp5BX1oRpDgf11OwF8RSLuhokj8VXE5esXD8xbzx9UixxoZw87dsZ9KQEeg4zcM5G9FA0oHIQ90/oLXp3Zv3XXf7mIfDzKh1ExtS0l59FFFEmpUPK5aXMuadZnsH31Zx3";

        //注：证书文件路径支持设置为文件系统中的路径或CLASS_PATH中的路径，优先从文件系统中加载，加载失败后会继续尝试从CLASS_PATH中加载
        config.merchantCertPath = "foo/appCertPublicKey_2021000116695722.crt";
        config.alipayCertPath = "foo/alipayCertPublicKey_RSA2.crt";
        config.alipayRootCertPath = "foo/alipayRootCert.crt";

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
//         config.alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArRdWbInkekKnTBF+YikAmW55lJcqeimjphZW8EZpM03u8WsItmxYO5BpBUivKsGm3FE1IFZYbik0ZH+WwhPhG+6fDp3jDQBprUkPmZCqBdc3J77AiPePUBD8sg+cSZzhJNTRqmAFbU8qMgY4QuqYpuZplfe+6FwhT2Bl6t+2gm2ULNVf5QW0pst4WRLfJrLVQYDXN85rza87AU+8RIC2NwD7v8+0EKy/2snsnsmO2R2TnW7gh39HWLwYFlyrgl0TPh5YSkCR2oH3j8Fs5b64PCFGu8nDZfArf9E/0UbpA+jaexzgquliiITVrkraE4BxUF3SX+xr0MOeavee/wu2sQIDAQAB";

        //可设置异步通知接收服务地址（可选）
//        config.notifyUrl = "<-- 请填写您的支付类接口异步通知接收服务地址，例如：https://www.test.com/callback -->";

        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
        config.encryptKey = "rn0sHR8Zj7beVAkFfWVaWg==";

        return config;
    }
}