package com.qianxi.wit.payment;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import org.junit.Test;

import java.io.File;

public class MyTest {
    @Test
    public void test() throws AlipayApiException {
        //构造client
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
    //设置网关地址
        certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
    //设置应用Id
        certAlipayRequest.setAppId("2021000116695722");
    //设置应用私钥
        certAlipayRequest.setPrivateKey("MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCVwI/s2opbnc2Pg1cdGXpH5JeCdbpAhWo//MWrvSAvzFxcCsmVDC42HehjO4/va6azvi1yq+at+qfjQQZ5RAxqUzJuhhdosaZrnQnIdvifQxkYqV+f/uudqyL1dLY+w15ZP1K02Q6+ZzighsQrmCvo4ip9fjm0vsC0VYVQuMx/sofa5zwmDTqU+tk9mJwt023xLBGzit9mUSZEKny6PKmJdqG9s3T+fRyyTKJBIsFW55n2zee2m/QhaR5+K9D7iX/AcI1pxvhU9nTfle8Bgsu6iHC2Yx0OwQyrbje15quY92Rc7A/q0mMHoZH1+3hMjpJLqOAQDCorP5imlAZ0tp2HAgMBAAECggEAflg51e7IPuXTsdpxHBcLyS4++g3Tt24Yp6XfLLdjOZCk9+8OBleBgZ0YHOj61lgwDIROmeoYtP6+uQh6qsNT6j0JVqCBjY4cx7x1rZFa0BvvR5RXHokYRy1FUGxI+okgyBGZ+QUM4G38I8uwJMu9eD2g9cISc8FgmoVw9hsERlOITS9VSJfS1Zog1niIXXMgvKGBWHo37NGM2fNuHeSgfqimwTyqTCx8fbwdyPAcm33SQYZLmWBmztEV6hhbXQ6S1E3h6RGQhRGFORABwOZ9ZUMZf0kXKF6Vcs2I7vVusV+X5s0P73Zz6zdqDKy59wE71kRNRnITRu0oNZI7yJ8U8QKBgQD7070DwKT0oQSlqVz+IqFLB9oa3OUtIJ83pO7AhXTY++Of79E8P6E5MH0svKvrthHsv8jDEvFgoz49FryJGqPEX1meZXukaUkH7repupioqi1Kxr7ZKRG8T/U2bzKKojcBrKF5i1gAcRaFU0VpmkTfOVV2xb2ReKVal3cNFzi27wKBgQCYO9FPfrRZd0ipiU8su2u0FRqU6XSyTMM+qTuKWHIYYmGkLxYJbJ1fevlPjooPUrLl64u84W3NsH6rLsOBZMLlsmouu8F2DJYhmvueyqNhuwB8qnbBjf7+t4KZbPj3qo5lOVczThKSeMkOXeOW3fqp3XTiAG5XGMzs0ww1J/nC6QKBgQCHRaIO1fjF/4iDhIqTaP7DUyBxtZ7VsuRT+6DUgdvWgtgvHZ+DgZqVIdrboS2onFDcn9i0EQTgKFBSeFgmPc0iuyVk1JaqJnQ4cp7t6XH/iAp0VHZIKKuCQGE0MF8x4RkzKQxEQfmvow+55pUuuNOuJRonhujhr7EDDMHComqwuQKBgCizhYjNhYaN/91kGskI6wjgw5SgvlSVdXYhZQveDdM7KoPtGuU2JvVKObHtmDf0kmQqj9fWJbdiwtsdy6OivwEOrJkGFflr8A/YEgIoh3ovtzedk9a4Ej8j7UT7/6yn6x5hMFFFRG3o4Uh4DcteWhBkZ0Wte9LJjLCc1Jy5rWtRAoGBAMlCDT28CH8fq4oYruXpTvqFbh+NP82PNZNVBagVo2IPp5BX1oRpDgf11OwF8RSLuhokj8VXE5esXD8xbzx9UixxoZw87dsZ9KQEeg4zcM5G9FA0oHIQ90/oLXp3Zv3XXf7mIfDzKh1ExtS0l59FFFEmpUPK5aXMuadZnsH31Zx3");
    //设置请求格式，固定值json
        certAlipayRequest.setFormat("json");
        //设置字符集
        certAlipayRequest.setCharset("UTF-8");
//设置签名类型
        certAlipayRequest.setSignType("RSA2");
//设置应用公钥证书路径
        certAlipayRequest.setCertPath("D:\\Java\\project\\IdeaProject\\PracticalTrainingOne\\Project\\wit-parent\\wit-payment\\src\\main\\resources\\foo\\appCertPublicKey_2021000116695722.crt");
//设置支付宝公钥证书路径
        certAlipayRequest.setAlipayPublicCertPath("D:\\Java\\project\\IdeaProject\\PracticalTrainingOne\\Project\\wit-parent\\wit-payment\\src\\main\\resources\\foo\\alipayCertPublicKey_RSA2.crt");
//设置支付宝根证书路径
        certAlipayRequest.setRootCertPath("D:\\Java\\project\\IdeaProject\\PracticalTrainingOne\\Project\\wit-parent\\wit-payment\\src\\main\\resources\\foo\\alipayRootCert.crt");
//构造client
        AlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);
//构造API请求
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();

        request.setBizContent( "{"  +
                "    \"out_trade_no\":\"20150320010101001\","  +
                "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\","  +
                "    \"total_amount\":88.88,"  +
                "    \"subject\":\"Iphone6 16G\","  +
                "    \"body\":\"Iphone6 16G\","  +
                "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\","  +
                "    \"extend_params\":{"  +
                "    \"sys_service_provider_id\":\"2088511833207846\""  +
                "    }" +
                "  }" ); //填充业务参数


//发送请求
        AlipayTradeQueryResponse response = alipayClient.certificateExecute(request);


    }
}
