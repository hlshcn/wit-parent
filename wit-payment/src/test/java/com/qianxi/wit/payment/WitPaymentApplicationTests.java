package com.qianxi.wit.payment;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = WitPaymentApplication.class)
class WitPaymentApplicationTests {

    @Test
    public void contextLoads() {
    }

}
