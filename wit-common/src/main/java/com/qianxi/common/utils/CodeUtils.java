package com.qianxi.common.utils;

import java.util.Random;

public class CodeUtils {
    /**
     * String生成制定随机位数
     *
     */
    public static String generateRandomCode(int num){
        //1.准备集合
        StringBuilder builder = new StringBuilder();
        //2.生成
        for (int i = 0; i < num; i++) {
            builder.append(new Random().nextInt(10));
        }
        //3.返回
        return builder.toString();
    }
}
