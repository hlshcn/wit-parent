package com.qianxi.common.utils;

import lombok.ToString;

import java.io.Serializable;

@ToString
public class Result implements Serializable {
    private int code;
    private String message;
    private Object data;

    private Result(){}

    private Result(CodeMessage codeMessage){
        this.code = codeMessage.getCode();
        this.message = codeMessage.getMessage();
    }

    private Result(CodeMessage codeMessage, Object data){
        this.code = codeMessage.getCode();
        this.message = codeMessage.getMessage();
        this.data = data;
    }

    private Result(int code, String message){
        this.code = code;
        this.message = message;
    }

    private Result(int code, String message, Object data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Result ok(){
        return new Result(200, "success");
    }

    public static Result ok(Object data){
        return new Result(200, "success", data);
    }

    public static Result error(CodeMessage codeMessage){
        return new Result(codeMessage);
    }

    public static Result error(int code, String message){
        return new Result(code, message);
    }

    public static Result error(int code, String message, Object data){
        return new Result(code, message, data);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

}
