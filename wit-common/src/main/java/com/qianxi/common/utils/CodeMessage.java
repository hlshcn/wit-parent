package com.qianxi.common.utils;

public class CodeMessage {
    private int code;
    private String message;

    private CodeMessage(int code, String message){
        this.code = code;
        this.message = message;
    }

    public static final CodeMessage SUCCESS = new CodeMessage(200, "success");
    public static final CodeMessage ERROR = new CodeMessage(500, "系统内部错误！");

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
