package com.qianxi.common.utils;

import java.util.Random;

/**
 * 用Math.random()工具类生成制定范围的数字
 *      n，m为整数，需要[n，m)
 *          Math.random() * (m - n) + n
 */
public class MathRandomDemo {
    public static void main(String[] args) {
        MathRandomDemo randomDemo = new MathRandomDemo();

        for (int i = 0; i < 10; i++) {
            System.out.println(randomDemo.generateRandomCode(5,10));
        }
    }

    /**
     * String生成制定随机位数
     *
     */
    public String generateRandomCode(int num){
        //1.准备集合
        StringBuilder builder = new StringBuilder();
        //2.生成
        for (int i = 0; i < num; i++) {
            builder.append(new Random().nextInt(10));
        }
        //3.返回
        return builder.toString();
    }

    /**
     * Math.random()生成制定范围整数int
     * [5, 10)
     *      Math.random()               [0, 1)
     *      Math.random()*(10-5)        [0, 5)
     *      Math.random()*(10-5)+5      [5, 10)
     */
    public int generateRandomCode(int start, int end){
//        return (int)(Math.random() * (end - start) + start);
        return new Random().nextInt(end - start) + 5;
    }
}
