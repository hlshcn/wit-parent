package com.qianxi.common.utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Random;

/*
pom.xml
<dependency>
  <groupId>com.aliyun</groupId>
  <artifactId>aliyun-java-sdk-core</artifactId>
  <version>4.5.3</version>
</dependency>
*/
public class SendSmsUtil {
    /**
     * 发送短信代码
     * @param phone     手机号
     * @param code   发送信息（验证码）
     * @return
     */
    public static String  sendSms(String phone, String code){
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4G2MkxeKiZ2CNDFG9zHN", "Y0yXV1gRLN0ePQH0j0br2mSqYfTXjr");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", "ABC商城");
        request.putQueryParameter("TemplateCode", "SMS_204970823");
        request.putQueryParameter("TemplateParam", "{\"code\":\""+code+"\"}");
        try {
//            CommonResponse response = client.getCommonResponse(request);
            System.out.println("发送的手机号：【"+phone+"】");
            System.out.println("发送的验证码：【"+code+"】");
//            System.out.println(response.getData());
            return "发送成功";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "发送失败！";
    }
	
	public static String randomCode() {
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            str.append(random.nextInt(10));
        }
        return str.toString();
    }

    public static void main(String[] args) {
        String msg = sendSms("17136831527", "666888");
//        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "<accessKeyId>", "<accessSecret>");
//        IAcsClient client = new DefaultAcsClient(profile);
//
//        CommonRequest request = new CommonRequest();
//        request.setSysMethod(MethodType.POST);
//        request.setSysDomain("dysmsapi.aliyuncs.com");
//        request.setSysVersion("2017-05-25");
//        request.setSysAction("SendSms");
//        request.putQueryParameter("RegionId", "cn-hangzhou");
//        request.putQueryParameter("PhoneNumbers", "17136831527");
//        request.putQueryParameter("SignName", "ABC商城");
//        request.putQueryParameter("TemplateCode", "SMS_204970823");
//        try {
//            CommonResponse response = client.getCommonResponse(request);
//            System.out.println(response.getData());
//        } catch (ServerException e) {
//            e.printStackTrace();
//        } catch (ClientException e) {
//            e.printStackTrace();
//        }
    }
}
