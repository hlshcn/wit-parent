package com.qianxi.wit.patient.vo;

import lombok.Data;

@Data
public class ConsultVo {
    private String symptom;
    private String description;
    private String hospitalName	;
    private String departmentName;
    private String  physicianName;
    private String location;

}
