package com.qianxi.wit.patient.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
public class LivingStreamingVo {
    private Long id;
    private String goodat;
    private String physicianName;
    private String hospitalName;
    private String departmentphysiciancategoryName;
    private String departmentName;

}
