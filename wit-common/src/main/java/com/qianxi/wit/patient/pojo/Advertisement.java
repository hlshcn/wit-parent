package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class Advertisement {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 广告标题
     */
    private String title;
    /**
     * 图片地址
     */
    private String image;
    /**
     * 是否生效(0未生效1生效
     */
    private String state;
    /**
     * 有效截止时间
     */
    private Date endTime;

}
