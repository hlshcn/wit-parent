package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 咨询表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Data
@TableName("tb_consult")
public class ConsultEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 咨询表id
	 */
	@TableId
	private Long id;
	/**
	 * 咨询时间
	 */
	private Date createtime;
	/**
	 * 医师id
	 */
	private Long physicianId;
	/**
	 * 医师姓名
	 */
	private String physicianName;
	/**
	 * 患者id
	 */
	private Long patientId;
	/**
	 * 患者姓名
	 */
	private String patientName;
	/**
	 * 咨询状态 （0 诊断结束，1诊断中）
	 */
	private Integer status;
	/**
	 * 咨询费用
	 */
	private BigDecimal price;
	/**
	 * 支付方式（0微信，1支付宝，2银行卡）
	 */
	private Integer paytype;

}
