package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 患者问诊记录表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_patient_record")
public class PatientRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 患者问诊记录id
	 */
	@TableId
	private Long id;
	/**
	 * 问诊时间
	 */
	private Date createtime;

	/**
	 * 患者的ID
	 */
	private String patientId;

	/**
	 * 患者姓名
	 */
	private String name;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 支付问诊金额
	 */
	private BigDecimal price;
	/**
	 * 病情
	 */
	private String patientCondition;
	/**
	 * 问诊医生姓名
	 */
	private String physicianName;
	/**
	 * 问诊医生职称
	 */
	private String categoryName;
	/**
	 * 问诊医生隶属医院
	 */
	private String hospitalName;

}
