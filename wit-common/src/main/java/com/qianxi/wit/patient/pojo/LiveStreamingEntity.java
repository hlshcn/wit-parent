package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 直播表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Data
@TableName("tb_live_streaming")
public class LiveStreamingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 直播表id
	 */
	@TableId
	private Long id;
	/**
	 * 擅长
	 */
	private String goodat;
	/**
	 * 医生id
	 */
	private Long physicianId;
	/**
	 * 医生名字
	 */
	private String physicianName;
	/**
	 * 状态
	 */
	private Integer status;

}
