package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 咨询明细表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Data
@TableName("tb_consult_record")
public class ConsultRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 咨询明细表id
	 */
	@TableId
	private Long id;
	/**
	 * 咨询表id
	 */
	private Long consultId;
	/**
	 * 发言时间
	 */
	private Date createtime;
	/**
	 * 发言内容
	 */
	private String contents;
	/**
	 * 发言人
 	 */
	private String spokesman;

}
