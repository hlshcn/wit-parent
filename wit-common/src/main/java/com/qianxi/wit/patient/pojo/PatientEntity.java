package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 患者表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_patient")
public class PatientEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 患者id
	 */
	@TableId
	private Long id;
	/**
	 * 患者手机号
	 */
	private String phone;
	/**
	 * 患者姓名
	 */
	private String name;
	/**
	 * 问诊次数
	 */
	private Long diagnosisTimes;
	/**
	 * 出生日期
	 */
	private Date birthday;
	/**
	 * 支付总额
	 */
	private BigDecimal totalPrice;
	/**
	 * 密码
	 */
	private String password;
}
