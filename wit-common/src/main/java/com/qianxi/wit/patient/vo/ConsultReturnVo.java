package com.qianxi.wit.patient.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


@Data
public class ConsultReturnVo {
    private Long id;//医师id
    private BigDecimal decimal;//咨询费用
    private String physicianName;//医师姓名
    private String categoryName;//医师级别
    private String hospitalName;//医院名称
    private String diseaseName;//擅长的病种
    private Integer commentNum;//服务次数
    private Double goodComment;//好评率

    public BigDecimal getDecimal() {
        return new BigDecimal(100);
    }
}
