package com.qianxi.wit.patient.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 常见病表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Data
@TableName("tb_common_disease")
public class CommonDiseaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 常见病表id
	 */
	@TableId
	private Long id;
	/**
	 * 常见病名
	 */
	private String name;

}
