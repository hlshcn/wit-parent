package com.qianxi.wit.examine.vo;

import lombok.Data;

@Data
public class PhysicianVo {
    private Long id;
    private String name;
    private String hospitalName;
    private String categoryName;
    private Integer years;
    private String state;
    private String doctorQualificationCertificate;
    private String physicianPracticingCertificate;
}
