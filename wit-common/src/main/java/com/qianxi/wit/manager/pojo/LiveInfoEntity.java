package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-21 11:51:14
 */
@Data
@TableName("tb_live_info")
public class LiveInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 直播名称
	 */
	private String name;
	/**
	 * 医生ID
	 */
	private Long physicianId;
	/**
	 * 医生姓名
	 */
	private String physicianName;
	/**
	 * 直播状态（0未开播1开播）
	 */
	private String state;
	/**
	 * 直播间创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	/**
	 * 预计直播结束时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	/**
	 * 直播结束时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date closeTime;
	/**
	 * 直播封面图
	 */
	private String image;
	/**
	 * 直播房间号
	 */
	private Long roomId;

}
