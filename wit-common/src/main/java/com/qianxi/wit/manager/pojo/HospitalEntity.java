package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 医院表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_hospital")
public class HospitalEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 医院名
	 */
	private String name;
	/**
	 * 医师数量
	 */
	private Long physicianNum;
	/**
	 * 问诊次数
	 */
	private Long diagnosisTimes;
	/**
	 * 评价次数
	 */
	private Long commentNum;
	/**
	 * 总分数
	 */
	private Double score;

}
