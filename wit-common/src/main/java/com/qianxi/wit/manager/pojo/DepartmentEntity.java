package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 科室表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_department")
public class DepartmentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 科室id
	 */
	@TableId
	private Long id;
	/**
	 * 医院id
	 */
	private Long hospitalId;
	/**
	 * 科室名称
	 */
	private String name;
	/**
	 * 问诊次数
	 */
	private Long diagnosisTimes;
	/**
	 * 评价次数
	 */
	private Long commentNum;
	/**
	 * 科室医师人数
	 */
	private Long physicianNum;
	/**
	 * 总分数
	 */
	private Double score;

}
