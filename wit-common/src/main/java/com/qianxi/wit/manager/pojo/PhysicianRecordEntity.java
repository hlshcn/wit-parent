package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 医生问诊记录表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_physician_record")
public class PhysicianRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 医生问诊记录id
	 */
	@TableId
	private Long id;
	/**
	 * 问诊时间
	 */
	private Date createtime;
	/**
	 * 收取问诊金额
	 */
	private BigDecimal price;
	/**
	 * 医师记录的病情
	 */
	private String physicianCondition;
	/**
	 * 医师ID
	 */
	private Long physicianId;
	/**
	 * 医生姓名
	 */
	private String physicianName;
	/**
	 * 医生等级
	 */
	private String categoryName;
	/**
	 * 医生隶属医院
	 */
	private String hospitalName;
	/**
	 * 患者ID
	 */
	private Long patientId;
	/**
	 * 患者姓名
	 */
	private String patientName;
	/**
	 * 年龄
	 */
	private Integer age;

}
