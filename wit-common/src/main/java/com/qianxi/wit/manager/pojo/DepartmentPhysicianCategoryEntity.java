package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 科室医生中间表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_department_physician_category")
public class DepartmentPhysicianCategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 中间表id
	 */
	@TableId
	private Long id;
	/**
	 * 级别
	 */
	private String name;
	/**
	 * 科室id
	 */
	private Long departmentsId;
	/**
	 * 医生id
	 */
	private Long physicianId;

}
