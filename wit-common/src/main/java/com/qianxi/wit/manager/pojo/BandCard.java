package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_bank_card")
public class BandCard {
    private static final long serialVersionUID = 1L;

    /**
     * 科室id
     */
    @TableId
    private Long id;
    /**
     * 医院id
     */
    private Long patientId;
    /**
     * 科室名称
     */
    private String bandCardId;
    /**
     * 问诊次数
     */
    private String cardDetails;
}
