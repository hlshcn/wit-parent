package com.qianxi.wit.manager.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Data
@TableName("tb_physician")
public class PhysicianEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 医师ID
	 */
	@TableId
	private Long id;
	/**
	 * 科室医生中间表ID
	 */
	private Long departmentPhysicianCategoryId;
	/**
	 * 医师名
	 */
	private String name;
	/**
	 * 年限
	 */
	private Integer years;
	/**
	 * 审核状态(0未审核1通过2未通过)
	 */
	private Integer state;
	/**
	 * 医师资格证
	 */
	private String doctorQualificationCertificate;
	/**
	 * 医师执业证书
	 */
	private String physicianPracticingCertificate;
	/**
	 * 身份证
	 */
	private String idCard;
	/**
	 * 总收入金额
	 */
	private BigDecimal totalRevenue;
	/**
	 * 问诊次数
	 */
	private Long diagnosisTimes;
	/**
	 * 评价次数
	 */
	private Long commentNum;
	/**
	 * 总分数
	 */
	private Double score;

}
