package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.manager.pojo.BandCard;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BandCardDao extends BaseMapper<BandCard> {

}
