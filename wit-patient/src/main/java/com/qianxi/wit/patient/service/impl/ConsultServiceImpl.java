package com.qianxi.wit.patient.service.impl;

import com.qianxi.wit.patient.dao.ConsultDao;
import com.qianxi.wit.patient.pojo.ConsultEntity;
import com.qianxi.wit.patient.service.ConsultService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("consultService")
public class ConsultServiceImpl extends ServiceImpl<ConsultDao, ConsultEntity> implements ConsultService {

}