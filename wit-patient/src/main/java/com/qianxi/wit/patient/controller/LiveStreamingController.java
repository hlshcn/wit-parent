package com.qianxi.wit.patient.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.service.LiveStreamingService;
import com.qianxi.wit.patient.vo.LivingStreamingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 直播表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@RestController
@RequestMapping
public class LiveStreamingController {
    @Autowired
    private LiveStreamingService liveStreamingService;

    //http://localhost:80/api/patient/findliving/1/3
    @GetMapping("/findliving/{page}/{size}")
    public Result findliving(
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ) {
        IPage<LivingStreamingVo> ipage = liveStreamingService.findliving(new Page<>(page, size));
        PageUtils pageUtils = new PageUtils(ipage.getRecords(), (int) ipage.getTotal(), (int) ipage.getSize(), (int) ipage.getCurrent());
        return Result.ok(pageUtils);

    }
}
