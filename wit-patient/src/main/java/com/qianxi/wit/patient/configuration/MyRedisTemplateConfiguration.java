package com.qianxi.wit.patient.configuration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 狂神说
 */
@Configuration
public class MyRedisTemplateConfiguration {
    /**
     * 配置的redisTemplate模板序列化信息
     * 企业中拿去可以直接使用（或者稍加修改）
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        //1. 我们为了开发方便，一般使用<String, Object>
        RedisTemplate<String, Object> template = new RedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);

        //2. Jackson序列化设置
        Jackson2JsonRedisSerializer<Object> jsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jsonRedisSerializer.setObjectMapper(objectMapper);

        //3. String的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        //4. 设置序列化
        // key采用String的方式序列化
        template.setKeySerializer(stringRedisSerializer);
        // hash采用String的方式序列化
        template.setHashKeySerializer(stringRedisSerializer);
        // values采用Jackson的方式序列化
        template.setValueSerializer(jsonRedisSerializer);
        // hash的value也采用Jackson的方式序列化
        template.setHashValueSerializer(jsonRedisSerializer);

        //5. 生效配置
        template.afterPropertiesSet();

        return template;
    }
}
