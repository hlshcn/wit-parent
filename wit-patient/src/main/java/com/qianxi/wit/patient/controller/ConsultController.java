package com.qianxi.wit.patient.controller;

import java.util.Arrays;
import java.util.Map;

import com.qianxi.wit.patient.service.ConsultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.R;



/**
 * 咨询表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@RestController
@RequestMapping("manager/consult")
public class ConsultController {
    @Autowired
    private ConsultService consultService;

}
