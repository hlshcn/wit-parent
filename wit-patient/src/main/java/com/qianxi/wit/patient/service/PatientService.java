package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.patient.vo.ConsultReturnVo;
import com.qianxi.wit.patient.pojo.PatientEntity;

import java.util.Map;


/**
 * 患者表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface PatientService extends IService<PatientEntity> {

    IPage<ConsultReturnVo> consultPhysicianList(Page<ConsultReturnVo> objectPage, Map<String, Object> map);
}

