package com.qianxi.wit.patient.configuration;

import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;
import com.qianxi.common.utils.Result;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PatientSentinelConfig {
    public PatientSentinelConfig(){
        WebCallbackManager.setUrlBlockHandler((request, response, e) -> {
            Result error = Result.error(500, "被限流了...", null);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().println(error);
        });
    }
}
