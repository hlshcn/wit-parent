package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.patient.pojo.PatientRecordEntity;

/**
 * 患者问诊记录表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface PatientRecordService extends IService<PatientRecordEntity> {

}

