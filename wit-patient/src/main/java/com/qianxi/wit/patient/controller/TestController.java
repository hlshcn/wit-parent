package com.qianxi.wit.patient.controller;

import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.feign.ManagerFeignService;
import com.qianxi.wit.patient.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
public class TestController {
    @Autowired
    private ManagerFeignService managerFeignService;
    @Autowired
    private TestService testService;

    @RequestMapping("/testService")
    public Result testService(){
        testService.testService();
        return Result.ok("testService....");
    }

    @RequestMapping("/patient/hospital/list")
    public Result patientHospitalList(){
        log.info("patient的patientHospitalList方法执行了...");
        Result result = managerFeignService.hospitalList();
        return result;
    }

    /**
     * 写入cookie
     */
    @RequestMapping("/write")
    public String writeCookie(HttpServletResponse response){
        Cookie cookie = new Cookie("patientCookie", "patientCookie");
//        cookie.setPath("/test");
        response.addCookie(cookie);
        return "cookie,write";
    }

    /**
     * 获取cookie
     */
    @RequestMapping("/test/read")
    public String readCookie(HttpServletRequest request){
        for (Cookie cookie : request.getCookies()) {
            System.out.println("=====================");
            System.out.println(cookie.getName()+"-"+cookie.getValue());
        }
        return "cookie,read";
    }

    /**
     * 测试gateway路由
     * @return
     */
    @GetMapping("/test/hello")
    public String hello(){
        return "patient,hello";
    }

    @GetMapping("/test/{id}")
    public String test(@PathVariable("id") String id){
        return "获取到的数据是：" + managerFeignService.hello(id);
    }
}
