package com.qianxi.wit.patient.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.pojo.ConsultRecordEntity;
import com.qianxi.wit.patient.pojo.PatientRecordEntity;
import com.qianxi.wit.patient.service.ConsultRecordService;
import com.qianxi.wit.patient.service.PatientRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 患者问诊记录表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class PatientRecordController {
    @Autowired
    private PatientRecordService patientRecordService;
    @Autowired
    private ConsultRecordService consultRecordService;

    /**
     * 2.患者诊断记录详情API
     * 根据传入的当前页，每页记录数来查询出带分页的患者信息列表，可包括的字段有登陆账号(患者手机号)、患者姓名、出生日期、问诊次数，支付总额，即可查询患者所有信息。
     * http://wise.infomation.com/api/patient/record/patient/{page}/{size}
     * @param patientRecordEntity   请求体参数（hospital_name 医院名称）
     * @param page                  当前页
     * @param size                  每页记录数
     * @return
     */
    @PostMapping("/record/patient/{patientId}/{page}/{size}")
    public Result findPatientRecordList(
            @RequestBody PatientRecordEntity patientRecordEntity,
            @PathVariable("patientId") Long patientId,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        // TODO 2.2 【广志】 患者诊断记录详情API
        //0.根据ID查询名称
        //1.分页信息
        IPage<PatientRecordEntity> iPage = patientRecordService.lambdaQuery()
                .eq(PatientRecordEntity::getPatientId, patientId)
                .like(!StringUtils.isEmpty(patientRecordEntity.getHospitalName()), PatientRecordEntity::getHospitalName, patientRecordEntity.getHospitalName())
                .page(new Page<>(page, size));
        //2.结果集
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }

    /**
     * 咨询明细表
     * @return
     */
    @RequestMapping("/record/consult/record/list/{consultId}/{page}/{size}")
    public Result consultRecordList(
            @PathVariable("consultId") Long consultId,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        //1.查询列表
        IPage<ConsultRecordEntity> iPage = consultRecordService.lambdaQuery()
                .eq(ConsultRecordEntity::getConsultId, consultId)
                .page(new Page<>(page, size));
        //2.封装分页信息
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }
}
