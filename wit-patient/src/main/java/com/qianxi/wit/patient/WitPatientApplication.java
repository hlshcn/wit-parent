package com.qianxi.wit.patient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class WitPatientApplication {

    public static void main(String[] args) {
        SpringApplication.run(WitPatientApplication.class, args);
    }

}
