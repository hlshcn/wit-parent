package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.vo.ConsultReturnVo;
import com.qianxi.wit.patient.dao.PatientDao;
import com.qianxi.wit.patient.pojo.PatientEntity;
import com.qianxi.wit.patient.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("patientService")
public class PatientServiceImpl extends ServiceImpl<PatientDao, PatientEntity> implements PatientService {
    @Autowired
    private PatientDao patientDao;
    @Override
    public IPage<ConsultReturnVo> consultPhysicianList(Page<ConsultReturnVo> objectPage, Map<String, Object> map) {
        return patientDao.consultPhysicianList(objectPage,map);
    }
}