package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.AdvertisementDao;
import com.qianxi.wit.patient.pojo.Advertisement;
import com.qianxi.wit.patient.service.AdvertisementService;
import org.springframework.stereotype.Service;

@Service
public class AdvertisementServiceImpl extends ServiceImpl<AdvertisementDao, Advertisement> implements AdvertisementService {
}
