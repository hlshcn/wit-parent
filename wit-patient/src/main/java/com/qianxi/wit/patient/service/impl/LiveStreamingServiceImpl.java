package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.LiveStreamingDao;
import com.qianxi.wit.patient.pojo.LiveStreamingEntity;
import com.qianxi.wit.patient.service.LiveStreamingService;
import com.qianxi.wit.patient.vo.LivingStreamingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("liveStreamingService")
public class LiveStreamingServiceImpl extends ServiceImpl<LiveStreamingDao, LiveStreamingEntity> implements LiveStreamingService {
    @Autowired
    private LiveStreamingDao liveStreamingDao;

    @Override
    public IPage<LivingStreamingVo> findliving(Page<LivingStreamingVo> page) {

        return liveStreamingDao.findliving(page);
    }
}