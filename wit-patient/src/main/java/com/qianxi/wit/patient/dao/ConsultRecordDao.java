package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.patient.pojo.ConsultRecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 咨询明细表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Mapper
public interface ConsultRecordDao extends BaseMapper<ConsultRecordEntity> {
	
}
