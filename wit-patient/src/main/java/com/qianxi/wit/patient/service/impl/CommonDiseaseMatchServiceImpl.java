package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.CommonDiseaseMatchDao;
import com.qianxi.wit.patient.pojo.CommonDiseaseMatchEntity;
import com.qianxi.wit.patient.service.CommonDiseaseMatchService;
import org.springframework.stereotype.Service;


@Service("commonDiseaseMatchService")
public class CommonDiseaseMatchServiceImpl extends ServiceImpl<CommonDiseaseMatchDao, CommonDiseaseMatchEntity> implements CommonDiseaseMatchService {

}