package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.patient.pojo.CommonDiseaseMatchEntity;

/**
 * 常见病匹配表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
public interface CommonDiseaseMatchService extends IService<CommonDiseaseMatchEntity> {

}

