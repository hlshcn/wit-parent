package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.ConsultRecordDao;
import com.qianxi.wit.patient.pojo.ConsultRecordEntity;
import com.qianxi.wit.patient.service.ConsultRecordService;
import org.springframework.stereotype.Service;


@Service("consultRecordService")
public class ConsultRecordServiceImpl extends ServiceImpl<ConsultRecordDao, ConsultRecordEntity> implements ConsultRecordService {

}