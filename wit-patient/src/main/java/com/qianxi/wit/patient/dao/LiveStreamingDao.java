package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.wit.patient.pojo.LiveStreamingEntity;
import com.qianxi.wit.patient.vo.LivingStreamingVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 直播表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Mapper
public interface LiveStreamingDao extends BaseMapper<LiveStreamingEntity> {



    IPage<LivingStreamingVo> findliving(Page<LivingStreamingVo> page);
}
