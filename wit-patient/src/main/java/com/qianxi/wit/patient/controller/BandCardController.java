package com.qianxi.wit.patient.controller;

import com.qianxi.common.utils.Result;
import com.qianxi.wit.manager.pojo.BandCard;
import com.qianxi.wit.patient.service.BandCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BandCardController {
    @Autowired
    private BandCardService bandCardService;

    @PostMapping("/band/card/save")
    public Result saveBandCard(@RequestBody BandCard bandCard) {
        //1.保存信息
        bandCard.setId(null);
        bandCardService.save(bandCard);
        return Result.ok();
    }
}
