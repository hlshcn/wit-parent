package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.patient.pojo.ConsultEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 咨询表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Mapper
public interface ConsultDao extends BaseMapper<ConsultEntity> {
	
}
