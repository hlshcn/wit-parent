package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.patient.pojo.Advertisement;

public interface AdvertisementService extends IService<Advertisement> {
}
