package com.qianxi.wit.patient.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("wit-payment")
public interface PaymentFeignService {
    /**
     * 支付接口
     * @param totalAmount
     * @param subject
     * @param body
     * @param outTradeNo
     * @return
     */
    @RequestMapping("/payPage")
    String payPage(
            @RequestParam String totalAmount,
            @RequestParam String subject,
            @RequestParam String body,
            @RequestParam String outTradeNo
    );
}
