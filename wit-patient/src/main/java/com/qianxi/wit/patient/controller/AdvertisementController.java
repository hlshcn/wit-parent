package com.qianxi.wit.patient.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.pojo.Advertisement;
import com.qianxi.wit.patient.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdvertisementController {
    @Autowired
    private AdvertisementService advertisementService;

    @RequestMapping("/advertisement/list/{page}/{size}")
    public Result advertisementList(
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        //1.查询列表
        IPage<Advertisement> iPage = advertisementService.lambdaQuery()
                .eq(Advertisement::getState, "1")
                .page(new Page<>(page, size));
        //2.封装分页
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int) iPage.getTotal(), (int) iPage.getSize(), (int) iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
