package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.wit.patient.vo.ConsultReturnVo;
import com.qianxi.wit.patient.pojo.PatientEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 患者表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Mapper
public interface PatientDao extends BaseMapper<PatientEntity> {

    IPage<ConsultReturnVo> consultPhysicianList(Page<ConsultReturnVo> objectPage, Map<String, Object> map);
}
