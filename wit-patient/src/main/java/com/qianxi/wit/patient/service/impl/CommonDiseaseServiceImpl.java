package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.CommonDiseaseDao;
import com.qianxi.wit.patient.pojo.CommonDiseaseEntity;
import com.qianxi.wit.patient.service.CommonDiseaseService;
import org.springframework.stereotype.Service;

@Service("commonDiseaseService")
public class CommonDiseaseServiceImpl extends ServiceImpl<CommonDiseaseDao, CommonDiseaseEntity> implements CommonDiseaseService {

}