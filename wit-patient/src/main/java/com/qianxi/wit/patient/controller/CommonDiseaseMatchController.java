package com.qianxi.wit.patient.controller;

import com.qianxi.wit.patient.service.CommonDiseaseMatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 常见病匹配表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@RestController
@RequestMapping("manager/commondiseasematch")
public class CommonDiseaseMatchController {
    @Autowired
    private CommonDiseaseMatchService commonDiseaseMatchService;

}
