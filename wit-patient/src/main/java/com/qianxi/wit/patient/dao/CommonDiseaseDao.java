package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.patient.pojo.CommonDiseaseEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 常见病表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@Mapper
public interface CommonDiseaseDao extends BaseMapper<CommonDiseaseEntity> {
	
}
