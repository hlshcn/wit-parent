package com.qianxi.wit.patient.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.CodeUtils;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.vo.ConsultReturnVo;
import com.qianxi.wit.patient.pojo.PatientEntity;
import com.qianxi.wit.patient.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 患者表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class PatientController {
    @Autowired
    private PatientService patientService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 1.患者管理列表API
     * 根据当前页(page)和每页记录数(size)来查询患者的详情信息，包括账号，姓名，年龄，问诊次数，支付总额等信息。
     * http://wise.infomation.com/api/patient/patient/{page}/{size}
     *
     * @param page 当期页
     * @param size 每页记录数
     * @return
     */
    @GetMapping("/patient/{page}/{size}")
    public Result findPatientList(
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ) {
        // TODO 2.1 【广志】 患者管理列表API
        //1.分页查询
        IPage<PatientEntity> iPage = patientService.page(new Page<>(page, size));
        //2.结果集
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int) iPage.getTotal(), (int) iPage.getSize(), (int) iPage.getCurrent());
        return Result.ok(pageUtils);
    }

    //获取验证码
    //TODO 获取验证码
    @GetMapping("/phone/code/{phone}")
    public Result getCerificationCode(
            @PathVariable("phone") String phone
    ) {
        String s = CodeUtils.generateRandomCode(6);
        System.out.println(s);
        redisTemplate.boundHashOps("cerificationCode").put(phone, s);
        redisTemplate.boundHashOps("cerificationCode").expire(1, TimeUnit.MINUTES);
        return Result.ok();
    }

    //登录加设置密码
    //TODO 验证登录
    @PostMapping("/phone/login/{phone}")
    public Result login(
            @PathVariable("phone") String phone,
            @RequestBody Map<String, String> map
    ) {
        String code = map.get("code");//验证码
        String password = map.get("password");//密码
        String o = (String) redisTemplate.boundHashOps("password").get(phone);//手机对应的密码
        if ((null == code || code.equals("")) && (password == null || password.equals(""))) {//都为空
            return Result.ok(false);
        }
        if (null != code && !code.equals("") && (password == null || password.equals(""))) {//验证码码不为空,密码为空
            String cerificationCode = (String) redisTemplate.boundHashOps("cerificationCode").get(phone);
            if (cerificationCode == code) {//判断验证码是否正确
                return Result.ok(true);
            } else {
                return Result.ok(false);
            }
        }
        if (null != password && !password.equals("") && (o == null || o.equals(""))) {//密码不为空，但手机号的密码为空
            redisTemplate.boundHashOps("password").put(phone, password);
            return Result.ok(true);
        }
        if (code != null && !code.equals("") && password != null && !password.equals("")) {//code和password 都不为空就让他失败
            return Result.ok(false);
        }
        if (null != o && !o.equals("") && !password.equals("") && password != null) {//判断密码登录
            if (o == password) {
                return Result.ok(true);
            } else {
                return Result.ok(false);
            }
        } else {
            return Result.ok(false);
        }
    }


    //符合条件的医师列表
    @PostMapping("/patient/physician/list/{page}/{size}")
    public Result consultPhysicianList(
            @RequestBody Map<String, Object> map,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ) {
        //TODO 符合条件的医师列表
        IPage<ConsultReturnVo> iPage = patientService.consultPhysicianList(new Page<ConsultReturnVo>(page, size), map);
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int) iPage.getTotal(), (int) iPage.getSize(), (int) iPage.getCurrent());
        return Result.ok(pageUtils);
    }

    @GetMapping("/patient/one/phone/password")
    public PatientEntity findPatientByPhoneAndPassword(String phone, String password) {
        //1.手机号密码不能为空
        if (!StringUtils.isEmpty(phone) || !StringUtils.isEmpty(password)) {
            return null;
        }
        //2.查询
        try {
            PatientEntity patientEntity = patientService.lambdaQuery()
                    .eq(PatientEntity::getPhone, phone)
                    .eq(PatientEntity::getPassword, password)
                    .list()
                    .get(0);
            return patientEntity;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/patient/one/phone/phone")
    PatientEntity findPatientByPhone(String phone) {
        try {
            return patientService.lambdaQuery().eq(PatientEntity::getPhone, phone).list().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/wer/ist")
    public Result pop() {
        String str = "ticket";
        String token = (String) redisTemplate.opsForValue().get("ticket");
        System.out.println(token);
        String phone = (String) redisTemplate.opsForValue().get("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI2NWRhZGZlNi05OGRhLTQxODAtOGU3MS0zYzk5NDAwNThmNWMiLCJzdWIiOiIxMzk5ODU0NjUyIiwiaWF0IjoxNjEwNjI4MTcxLCJyb2xlcyI6IjEyMzQ1NiIsImV4cCI6MTYxMDYyODE3MX0.VMDOLX_wo_T85m9X1K1kdyDc-tQcua1emz_cGugUYfU");

        List<PatientEntity> list = patientService.query().eq("phone", phone).list();

        if (list.size() < 1 && list == null) {
            return Result.ok("你需要登陆");
        } else {
//            PatientEntity liste = patientService.query().eq("phone", phone).list().get(0);
            PatientEntity liste = patientService.query().eq("phone", "1399854652").list().get(0);
            return Result.ok(liste);
        }
    }

    @RequestMapping("/patient/record/consult/record/{phone}/{pwd}")
    public Result Changepassword(
            @PathVariable("phone") String phone,
            @PathVariable("pwd") String pwd,
            @RequestBody Map<String, String> map
    ) {
        String password = map.get("password");//密码
        //根据手机号获取密码
        //1.查询 phone 数据库
        PatientEntity patientEntity = patientService.query()
                .eq("phone", phone).list().get(0);
        if (patientEntity.getPassword().equals(pwd)) {
            //2.修改password
            patientEntity.setPassword(password);
            //3.把最新entity返回数据库
            patientService.updateById(patientEntity);
            return Result.ok(true);
        }
        return Result.ok(true);
    }

    @RequestMapping("/test")
    public String test() {
        List<Long> collect = patientService.listObjs(
                new QueryWrapper<PatientEntity>()
                .eq("id", 1)
                .select("id")
                ,
                o -> Long.parseLong(o.toString())
        );
        collect.stream().forEach(System.out::println);
        return "hello";
    }
}
