package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.patient.pojo.LiveStreamingEntity;
import com.qianxi.wit.patient.vo.LivingStreamingVo;

import java.util.Map;

/**
 * 直播表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
public interface LiveStreamingService extends IService<LiveStreamingEntity> {


    IPage<LivingStreamingVo> findliving(Page<LivingStreamingVo> page);
}

