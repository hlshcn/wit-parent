package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.patient.pojo.PatientRecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 患者问诊记录表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Mapper
public interface PatientRecordDao extends BaseMapper<PatientRecordEntity> {
	
}
