package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.manager.pojo.BandCard;
import com.qianxi.wit.patient.dao.BandCardDao;
import com.qianxi.wit.patient.service.BandCardService;
import org.springframework.stereotype.Service;

@Service
public class BandCardServiceImpl extends ServiceImpl<BandCardDao, BandCard> implements BandCardService {
}
