package com.qianxi.wit.patient.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.manager.pojo.BandCard;

public interface BandCardService extends IService<BandCard> {
}
