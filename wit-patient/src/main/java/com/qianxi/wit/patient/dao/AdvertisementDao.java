package com.qianxi.wit.patient.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qianxi.wit.patient.pojo.Advertisement;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdvertisementDao extends BaseMapper<Advertisement> {

}
