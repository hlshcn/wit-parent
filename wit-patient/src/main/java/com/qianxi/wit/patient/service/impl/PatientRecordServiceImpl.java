package com.qianxi.wit.patient.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.patient.dao.PatientRecordDao;
import com.qianxi.wit.patient.pojo.PatientRecordEntity;
import com.qianxi.wit.patient.service.PatientRecordService;
import org.springframework.stereotype.Service;


@Service("patientRecordService")
public class PatientRecordServiceImpl extends ServiceImpl<PatientRecordDao, PatientRecordEntity> implements PatientRecordService {


}