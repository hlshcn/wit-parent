package com.qianxi.wit.patient.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.qianxi.wit.patient.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
    @SentinelResource(value = "testServiceResource", blockHandler = "testServiceResource")
    @Override
    public void testService() {
        System.out.println("TestServiceImpl testService 正常执行....");
    }

    public void testServiceResource(BlockException ex) {
        System.out.println("被降级..." + ex.getMessage());
    }

}
