package com.qianxi.wit.patient.feign;

import com.qianxi.common.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "wit-manager",fallback = ManagerFeignServiceFallbak.class)
public interface ManagerFeignService {

    @GetMapping("/test/hello/{id}")
    String hello(@PathVariable("id") String id);

    @RequestMapping("/hospital/list")
    Result hospitalList();
}

class ManagerFeignServiceConfig{
    @Bean
    public ManagerFeignServiceFallbak managerFeignServiceFallbak(){
        return new ManagerFeignServiceFallbak();
    }
}

@Component
class ManagerFeignServiceFallbak implements ManagerFeignService{

    @Override
    public String hello(String id) {
        return null;
    }

    @Override
    public Result hospitalList() {
        return Result.error(500, "服务熔断...", null);
    }
}
