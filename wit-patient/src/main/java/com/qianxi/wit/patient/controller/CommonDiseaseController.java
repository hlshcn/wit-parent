package com.qianxi.wit.patient.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.patient.pojo.CommonDiseaseEntity;
import com.qianxi.wit.patient.service.CommonDiseaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.R;



/**
 * 常见病表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-12 19:04:23
 */
@RestController
public class CommonDiseaseController {
    @Autowired
    private CommonDiseaseService commonDiseaseService;

    /**
     * 10.获取所有常见病信息
     * @param page  当前页
     * @param size  每页记录数
     * @return
     */
    @GetMapping("/common/disease/list/{page}/{size}")
    public Result commonDiseaseList(@PathVariable("page") int page, @PathVariable("size") int size){
        // TODO 2.10.获取所有常见病信息
        //1.分页查询
        IPage<CommonDiseaseEntity> iPage = commonDiseaseService.lambdaQuery()
                .page(new Page<>(page, size));
        //2.封装返回对象
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getSize());
        return Result.ok(pageUtils);
    }
}
