package com.qianxi.wit.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommunicationController {
    @RequestMapping("/hello")
    public String hello(){
        return "communication,hello";
    }
}
