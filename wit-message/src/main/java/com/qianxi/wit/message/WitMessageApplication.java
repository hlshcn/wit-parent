package com.qianxi.wit.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class WitMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(WitMessageApplication.class, args);
    }

}
