package com.qianxi.wit.message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/message")
public class SmsController {
    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/sms")
    public void sendSms(@RequestBody Map map) {
        //1.发送短信到消息队列
        jmsTemplate.convertAndSend("sms", map);
    }
}
