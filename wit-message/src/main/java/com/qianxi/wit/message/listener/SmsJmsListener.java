package com.qianxi.wit.message.listener;

import com.qianxi.common.utils.SendSmsUtil;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class SmsJmsListener {
    @JmsListener(destination = "sms")
    public void sendSms(Map map){
        //1.解析到手机号和验证码
        String phone = (String) map.get("phone");
        String code = (String) map.get("code");
        //2.发送验证码
        SendSmsUtil.sendSms(phone, code);
    }
}
