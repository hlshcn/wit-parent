package com.qianxi.wit.examine.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.wit.examine.pojo.PhysicianEntity;
import com.qianxi.wit.examine.vo.PhysicianVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 医师表
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Mapper
public interface PhysicianDao extends BaseMapper<PhysicianEntity> {

    IPage<PhysicianVo> findPhysicianListByState(Page<PhysicianVo> page, @Param("map") Map map, @Param("state") String state);
}
