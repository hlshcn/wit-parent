package com.qianxi.wit.examine.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.examine.pojo.PhysicianEntity;
import com.qianxi.wit.examine.vo.PhysicianVo;

import java.util.Map;

/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface PhysicianService extends IService<PhysicianEntity> {


    IPage<PhysicianVo> findPhysicianListByState(Page<PhysicianVo> mapPage, Map map, String state);
}

