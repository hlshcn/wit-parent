package com.qianxi.wit.examine.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.examine.vo.PhysicianVo;
import com.qianxi.wit.examine.service.PhysicianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class PhysicianController {
    @Autowired
    private PhysicianService physicianRecordService;

    /**
     * 1.医师审核列表API
     * 通过审核状态（未审核，通过，未通过）状态，以及医生职称，隶属医院，从医年限等参数来进行分页查询医生列表信息。
     * http://wise.infomation.com/api/examine/physician/{state}/{page}/{size}
     * @param map               请求体参数信息（hospitalName，categoryName，years）
     * @param state             请求状态
     * @param page              当前页
     * @param size              每页记录数
     * @return
     */
    @PostMapping("/physician/{state}/{page}/{size}")
    public Result findPhysicianListByState(
//            @RequestBody PhysicianEntity physicianRecordEntity,
            @RequestBody Map map,
            @PathVariable("state") String state,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        // TODO 3.1 【丁宁，佳圆】 医师审核列表API
        //1.查询
        IPage<PhysicianVo> iPage = physicianRecordService.findPhysicianListByState(new Page<>(page, size), map, state);
        //2.分页
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
