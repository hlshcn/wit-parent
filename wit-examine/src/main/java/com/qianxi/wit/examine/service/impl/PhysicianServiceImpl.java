package com.qianxi.wit.examine.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.wit.examine.dao.PhysicianDao;
import com.qianxi.wit.examine.pojo.PhysicianEntity;
import com.qianxi.wit.examine.vo.PhysicianVo;
import com.qianxi.wit.examine.service.PhysicianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("physicianService")
public class PhysicianServiceImpl extends ServiceImpl<PhysicianDao, PhysicianEntity> implements PhysicianService {
    @Autowired
    private PhysicianDao physicianDao;

    @Override
    public IPage<PhysicianVo> findPhysicianListByState(Page<PhysicianVo> page, Map map, String state) {
        return physicianDao.findPhysicianListByState(page, map, state);
    }
}