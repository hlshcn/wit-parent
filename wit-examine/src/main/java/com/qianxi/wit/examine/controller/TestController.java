package com.qianxi.wit.examine.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    /**
     * 测试gateway路由
     * @return
     */
    @GetMapping("/test/hello")
    public String hello(){
        return "examine,hello";
    }
}
