package com.qianxi.wit.sso.controller;

import com.qianxi.wit.sso.feign.PatientFeignServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {
    @Autowired
    private PatientFeignServer patientFeignServer;

    /**
     * 获取cookie
     */
    @RequestMapping("/read")
    public String readCookie(HttpServletRequest request){
        for (Cookie cookie : request.getCookies()) {
            System.out.println("=====================");
            System.out.println(cookie.getName()+"-"+cookie.getValue());
        }
        return "cookie,read";
    }
}
