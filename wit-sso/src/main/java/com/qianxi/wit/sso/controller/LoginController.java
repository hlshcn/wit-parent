package com.qianxi.wit.sso.controller;

import com.qianxi.common.utils.CodeUtils;
import com.qianxi.common.utils.JwtUtil;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.examine.pojo.PhysicianEntity;
import com.qianxi.wit.patient.pojo.PatientEntity;
import com.qianxi.wit.sso.feign.MessageFeignService;
import com.qianxi.wit.sso.feign.PatientFeignServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class LoginController {
    @Autowired
    private MessageFeignService messageFeignService;
    @Autowired
    private PatientFeignServer patientFeignServer;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 根据手机号去获取验证码
     */
    @GetMapping("/phone/code/{phone}")
    public Result getCode(@PathVariable("phone") String phone) {
        //1.获取验证码
        String code = CodeUtils.generateRandomCode(6);
        //2.将验证码存入redis
        redisTemplate.opsForValue().set("phone:" + phone, code, 5, TimeUnit.MINUTES);
        //3.异步发送短信
        Map map = new HashMap();
        map.put("phone", phone);
        map.put("code", code);
        messageFeignService.sendSms(map);
        return Result.ok();
    }

    /**
     * 验证登录信息
     * 传入phone和password
     */
    @RequestMapping("/phone/login/{phone}")
    public Result login(@RequestBody Map map, @PathVariable("phone") String phone, HttpServletRequest request, HttpServletResponse response) {
        try {
            //0.获取code和password
            String code = (String) map.get("code");
            String password = (String) map.get("password");

            //1.验证用户名和密码的正确性
            PatientEntity patientEntity = null;
            if (!StringUtils.isEmpty(code)) {
                //验证code
                //1.1获取code
                String redisCode = (String) redisTemplate.opsForValue().get("phone:" + phone);
                //1.2验证
                if (!StringUtils.isEmpty(redisCode)) {
//                    patientEntity = patientFeignServer.findPatientByPhone(phone);
                    if (redisCode.equals(code)) {
                        return Result.ok(patientEntity);
                    }
                    return Result.error(500, "验证码错误!");
                }
            }
            if (!StringUtils.isEmpty(password)) {
                //验证phone和password
                patientEntity = patientFeignServer.findPatientByPhoneAndPassword(phone, password);
            }
            if (patientEntity == null) {
                return Result.error(500, "验证失败！请核对手机号和验证码(密码)", false);
            }

            //2.正确开始生成token
            JwtUtil jwtUtil = new JwtUtil();
            String token = jwtUtil.createJWT(patientEntity.getId() + "", patientEntity.getName(), "role");
            //3.把token存入cookie
            Cookie cookie = new Cookie("token", token);
            response.addCookie(cookie);
            //4.把token存入redis
            redisTemplate.opsForValue().set(token, phone, 15, TimeUnit.MINUTES);
            return Result.ok(true);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.ok(true);
        }
    }

    /**
     * 退出登录
     *
     * @return
     */
    @RequestMapping("/phone/logout")
    public Result logout(HttpServletRequest request) {
        try {
            //0.根据cookie查询token
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("token")) {
                    //1.退出登录
                    redisTemplate.delete(cookie.getName());
                }
            }
            return Result.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.ok();
        }
    }

    @RequestMapping("/test")
    public String test() {
        String phone = "13133334444";
        return (String) redisTemplate.opsForValue().get("phone:" + phone);
    }
}
