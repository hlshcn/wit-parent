package com.qianxi.wit.sso.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient("wit-message")
public interface MessageFeignService {

    /**
     * 发送短信
     * @param map
     */
    @PostMapping("/message/sms")
    public void sendSms(@RequestBody Map map);
}
