package com.qianxi.wit.sso.feign;

import com.qianxi.wit.patient.pojo.PatientEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("wit-patient")
public interface PatientFeignServer {
    @GetMapping("/test/hello")
    String hello();

    @GetMapping("/patient/one/phone/password")
    PatientEntity findPatientByPhoneAndPassword(@RequestParam String phone, @RequestParam String password);

    @GetMapping("/patient/one/phone/phone")
    PatientEntity findPatientByPhone(@RequestParam String phone);
}
