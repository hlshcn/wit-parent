package com.qianxi.wit.gateway.config;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;
import com.alibaba.fastjson.JSON;
import com.qianxi.common.utils.Result;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Configuration
public class GatewaySentinelConfig {
    /**
     * 构造器，初始化自定义返回信息
     */
    public GatewaySentinelConfig(){
        GatewayCallbackManager.setBlockHandler((exchange, t) -> {
            Result error = Result.error(500, "gateway限流...", null);
            Mono<ServerResponse> body = ServerResponse.ok().body(Mono.just(JSON.toJSONString(error)), String.class);
            return body;
        });
    }
}
