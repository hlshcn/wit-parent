package com.qianxi.wit.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.manager.pojo.PhysicianEntity;

import java.util.List;
import java.util.Map;

/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface PhysicianService extends IService<PhysicianEntity> {
    List<Map> findPhysicianCategoryDetailsByDepartmentId(Long departmentId);
}

