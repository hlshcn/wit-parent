package com.qianxi.wit.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qianxi.wit.manager.service.DepartmentPhysicianCategoryService;


/**
 * 科室医生中间表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
@RequestMapping("manager/departmentphysiciancategory")
public class DepartmentPhysicianCategoryController {
    @Autowired
    private DepartmentPhysicianCategoryService service;

}
