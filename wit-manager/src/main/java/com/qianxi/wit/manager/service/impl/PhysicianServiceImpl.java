package com.qianxi.wit.manager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.qianxi.wit.manager.dao.PhysicianDao;
import com.qianxi.wit.manager.pojo.PhysicianEntity;
import com.qianxi.wit.manager.service.PhysicianService;


@Service("physicianService")
public class PhysicianServiceImpl extends ServiceImpl<PhysicianDao, PhysicianEntity> implements PhysicianService {
    @Autowired
    private PhysicianDao physicianDao;

    @Override
    public List<Map> findPhysicianCategoryDetailsByDepartmentId(Long departmentId) {
        return physicianDao.findPhysicianCategoryDetailsByDepartmentId(departmentId);
    }
}