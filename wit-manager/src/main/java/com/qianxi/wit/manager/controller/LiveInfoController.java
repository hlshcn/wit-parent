package com.qianxi.wit.manager.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qianxi.wit.manager.pojo.LiveInfoEntity;
import com.qianxi.wit.manager.service.LiveInfoService;
import com.qianxi.common.utils.PageUtils;
import org.springframework.web.multipart.MultipartFile;


/**
 * 
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-21 11:51:14
 */
@RestController
public class LiveInfoController {
    @Autowired
    private LiveInfoService liveInfoService;

    private static String url = ""; // 文件地址

    @RequestMapping("/manager/upload/image")
    public String uploadImage(MultipartFile file, LiveInfoEntity liveInfoEntity) throws IOException {
        //1.获取文件名
        String originalFilename = file.getOriginalFilename();
        File imageFile = new File("D:\\Java\\project\\IdeaProject\\PracticalTrainingOne\\Project\\wit-parent\\wit-manager\\src\\main\\resources\\static\\images\\" + originalFilename);
        file.transferTo(imageFile);
        //2.添加记录信息
        liveInfoEntity.setId(null);
        liveInfoEntity.setImage("http://localhost:11000/images/"+originalFilename);
        liveInfoEntity.setCreateTime(new Date());
        liveInfoEntity.setState("0");
        //3.开始添加直播间信息
        liveInfoService.save(liveInfoEntity);
        System.out.println("添加成功！");
        return "添加成功！";
    }

    /**
     * 查询直播列表分页信息
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/manager/live/list/{state}/{page}/{size}")
    public Result liveList(
            @PathVariable("state") String state,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        //1.分页查询
        IPage<LiveInfoEntity> iPage = liveInfoService.lambdaQuery()
                .eq(LiveInfoEntity::getState, state)
                .page(new Page<>(page, size));
        //2.封装信息
        PageUtils pageUtils = new PageUtils(iPage);
        return Result.ok(pageUtils);
    }

    @RequestMapping("/manager/update/state/{state}/{roomId}")
    public Result updateStateByRoomId(@PathVariable("state") String state, @PathVariable("roomId") String roomId){
        boolean update = liveInfoService.lambdaUpdate()
                .set(LiveInfoEntity::getState, state)
                .eq(LiveInfoEntity::getRoomId, roomId)
                .update();
        return Result.ok(update);
    }
}
