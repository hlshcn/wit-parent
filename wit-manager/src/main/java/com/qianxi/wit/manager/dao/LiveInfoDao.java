package com.qianxi.wit.manager.dao;

import com.qianxi.wit.manager.pojo.LiveInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-21 11:51:14
 */
@Mapper
public interface LiveInfoDao extends BaseMapper<LiveInfoEntity> {
	
}
