package com.qianxi.wit.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.manager.pojo.DepartmentPhysicianCategoryEntity;

/**
 * 科室医生中间表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface DepartmentPhysicianCategoryService extends IService<DepartmentPhysicianCategoryEntity> {

}

