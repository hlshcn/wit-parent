package com.qianxi.wit.manager.dao;

import com.qianxi.wit.manager.pojo.PhysicianEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Mapper
public interface PhysicianDao extends BaseMapper<PhysicianEntity> {

    List<Map> findPhysicianCategoryDetailsByDepartmentId(Long departmentId);
}
