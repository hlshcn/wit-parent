package com.qianxi.wit.manager.dao;

import com.qianxi.wit.manager.pojo.DepartmentPhysicianCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 科室医生中间表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Mapper
public interface DepartmentPhysicianCategoryDao extends BaseMapper<DepartmentPhysicianCategoryEntity> {

}
