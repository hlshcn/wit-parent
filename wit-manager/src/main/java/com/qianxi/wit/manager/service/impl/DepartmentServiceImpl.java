package com.qianxi.wit.manager.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.qianxi.wit.manager.dao.DepartmentDao;
import com.qianxi.wit.manager.pojo.DepartmentEntity;
import com.qianxi.wit.manager.service.DepartmentService;


@Service("departmentService")
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDao, DepartmentEntity> implements DepartmentService {


}