package com.qianxi.wit.manager.controller;

import com.qianxi.common.utils.Result;
import com.qianxi.wit.manager.feign.ExamineFeignService;
import com.qianxi.wit.manager.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private HospitalService service;
    @Autowired
    private ExamineFeignService feignService;

    @GetMapping("/hello/{id}")
    public String hello(@PathVariable("id") String id){
        return "manager,hello,id:"+id;
    }

    /**
     * 测试调用examine远程服务
     */
    @GetMapping("/examine/test/hello")
    public String examineTestHello(){
        return feignService.hello();
    }

    @GetMapping("/hospital/list")
    public Object hospitalList(){
        return Result.ok(service.list());
    }
}
