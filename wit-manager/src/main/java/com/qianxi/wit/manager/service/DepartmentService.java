package com.qianxi.wit.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.manager.pojo.DepartmentEntity;

/**
 * 科室表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface DepartmentService extends IService<DepartmentEntity> {

}

