package com.qianxi.wit.manager.controller;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.qianxi.wit.manager.pojo.PhysicianEntity;
import com.qianxi.wit.manager.service.PhysicianService;
import com.qianxi.common.utils.PageUtils;


/**
 * 医师表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class PhysicianController {
    @Autowired
    private PhysicianService physicianService;

    /**
     * 3.某科室下医师分类信息API
     * http://wise.infomation.com/api/manager/physician/statistics/{departmentId}
     * 根据某医院下具体的科室(departmentId)，来查询统计其主治医师、专家医师、普通医师职位的医生人数、好评度、问诊人数等信息。
     * @param departmentId
     * @return
     */
    @GetMapping("/physician/statistics/{departmentId}")
    public Result findPhysicianCategoryDetailsByDepartmentId(
            @PathVariable("departmentId") Long departmentId
    ) {
        // TODO 1.3 【丁宁，佳圆】 某科室下医师分类信息API
        //0.计算当前分页信息
        //1.根据部门id查询出信息
        List<Map> maps = physicianService.findPhysicianCategoryDetailsByDepartmentId(departmentId);
        //2.封装
        return Result.ok(maps);
    }

    /**
     * 4.医师职位详情API
     * 根据职位名称（如：主治医师）查询在其位置想详情信息
     * （医师名称、等级、年限、资格证书、职业证书、访问次数、总收入、好评度）
     * http://wise.infomation.com/api/manager/physician/{departmentPhysicianCategoryId}/{page}/{size}
     * @param physicianEntity
     * @param departmentPhysicianCategoryId
     * @param page
     * @param size
     * @return
     */
    @PostMapping("/physician/{departmentPhysicianCategoryId}/{page}/{size}")
    public Result findPhysicianDetailsByDepartmentPhysicianCategoryId(
            @RequestBody PhysicianEntity physicianEntity,
            @PathVariable("departmentPhysicianCategoryId") Long departmentPhysicianCategoryId,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        // TODO 1.4 【丁宁，佳圆】 医师职位详情API
        //1.查询信息
        IPage<PhysicianEntity> iPage = physicianService.lambdaQuery()
                .eq(PhysicianEntity::getState, "1")
                .eq(PhysicianEntity::getDepartmentPhysicianCategoryId, departmentPhysicianCategoryId)
                .like(!StringUtils.isEmpty(physicianEntity.getName()), PhysicianEntity::getName, physicianEntity.getName())
                .page(new Page<>(page, size));
        //2.分页信息整合
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int) iPage.getTotal(), (int) iPage.getSize(), (int) iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
