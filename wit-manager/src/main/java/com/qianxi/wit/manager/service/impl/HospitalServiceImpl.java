package com.qianxi.wit.manager.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.qianxi.wit.manager.dao.HospitalDao;
import com.qianxi.wit.manager.pojo.HospitalEntity;
import com.qianxi.wit.manager.service.HospitalService;


@Service("hospitalService")
public class HospitalServiceImpl extends ServiceImpl<HospitalDao, HospitalEntity> implements HospitalService {

}