package com.qianxi.wit.manager.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.qianxi.wit.manager.dao.PhysicianRecordDao;
import com.qianxi.wit.manager.pojo.PhysicianRecordEntity;
import com.qianxi.wit.manager.service.PhysicianRecordService;


@Service("physicianRecordService")
public class PhysicianRecordServiceImpl extends ServiceImpl<PhysicianRecordDao, PhysicianRecordEntity> implements PhysicianRecordService {


}