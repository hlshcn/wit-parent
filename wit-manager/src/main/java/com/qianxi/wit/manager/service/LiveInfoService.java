package com.qianxi.wit.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.wit.manager.pojo.LiveInfoEntity;

import java.util.Map;

/**
 * 
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-21 11:51:14
 */
public interface LiveInfoService extends IService<LiveInfoEntity> {

}

