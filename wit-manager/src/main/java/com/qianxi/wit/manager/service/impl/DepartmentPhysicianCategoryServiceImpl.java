package com.qianxi.wit.manager.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.qianxi.wit.manager.dao.DepartmentPhysicianCategoryDao;
import com.qianxi.wit.manager.pojo.DepartmentPhysicianCategoryEntity;
import com.qianxi.wit.manager.service.DepartmentPhysicianCategoryService;


@Service("departmentPhysicianCategoryService")
public class DepartmentPhysicianCategoryServiceImpl extends ServiceImpl<DepartmentPhysicianCategoryDao, DepartmentPhysicianCategoryEntity> implements DepartmentPhysicianCategoryService {


}