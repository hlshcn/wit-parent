package com.qianxi.wit.manager.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("wit-examine")
public interface ExamineFeignService {
    /**
     * examine远程调用服务测试【成功】
     * @return
     */
    @GetMapping("/test/hello")
    String hello();
}
