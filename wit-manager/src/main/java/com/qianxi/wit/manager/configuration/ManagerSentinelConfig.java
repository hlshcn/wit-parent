package com.qianxi.wit.manager.configuration;

import com.alibaba.csp.sentinel.adapter.servlet.callback.WebCallbackManager;
import com.qianxi.common.utils.R;
import com.qianxi.common.utils.Result;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ManagerSentinelConfig {
    public ManagerSentinelConfig(){
        WebCallbackManager.setUrlBlockHandler((request, response, e) -> {
            Result error = Result.error(500, "被限流了...", null);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().println(error);
        });
    }
}
