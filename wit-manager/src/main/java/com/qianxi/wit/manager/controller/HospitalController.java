package com.qianxi.wit.manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.qianxi.wit.manager.pojo.HospitalEntity;
import com.qianxi.wit.manager.service.HospitalService;
import com.qianxi.common.utils.PageUtils;

import java.util.List;

/**
 * 医院表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@Slf4j
@RestController
public class HospitalController {
    @Autowired
    private HospitalService hospitalService;

    @RequestMapping("/hospital/list")
    public Result hospitalList(){
        List<HospitalEntity> list = hospitalService.list();
        log.info("医院列表方法执行了...");
        return Result.ok(list);
    }

    /**
     * 1.医院列表API
     * 根据指定的当前页(page)和每页记录数(size)，并且可以再请求体中携带要搜索的医院名称(name)来返回分页的医院列表信息。
     * http://wise.infomation.com/api/manager/hospital/{page}/{size}
     *
     * @param hospitalEntity 请求参数(name)
     * @param page           当前页
     * @param size           每页记录数
     * @return
     */
    @PostMapping("/hospital/{page}/{size}")
    public Result hospitalList(
            @RequestBody HospitalEntity hospitalEntity,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ) {
        // TODO 1.1 【申玮】 医院列表API
        //1.分页模糊查询
        IPage<HospitalEntity> iPage = hospitalService.lambdaQuery()
                .like(!StringUtils.isEmpty(hospitalEntity.getName()), HospitalEntity::getName, hospitalEntity.getName())
                .page(new Page<>(page, size));
        //2.封装到PageUtils
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
