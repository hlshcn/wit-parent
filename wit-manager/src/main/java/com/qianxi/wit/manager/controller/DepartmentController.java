package com.qianxi.wit.manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.PageUtils;
import com.qianxi.common.utils.Result;
import com.qianxi.wit.manager.pojo.DepartmentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.qianxi.wit.manager.service.DepartmentService;

/**
 * 科室表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    /**
     * 2.科室列表API
     * http://wise.infomation.com/api/manager/department/{hospitalId}/{page}/{size}
     * 根据传入的当前页，每页记录数和查询条件（科室名,医院id等）来查询出带分页的科室信息列表，
     * 可包括的字段有科室名、医生人数、好评度、问诊次数等，传入医院id即可查询某医院下的所有科室信息。
     *
     * @param departmentEntity 请求体参数
     * @param hospitalId       医院ID
     * @param page             当期页
     * @param size             每页记录数
     * @return
     */
    @PostMapping("/department/{hospitalId}/{page}/{size}")
    public Result departmentList(
            @RequestBody DepartmentEntity departmentEntity,
            @PathVariable("hospitalId") Long hospitalId,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ) {
        // TODO 1.2 【申玮】 科室列表API
        //1.条件分页查询
        IPage<DepartmentEntity> iPage = departmentService.lambdaQuery()
                .eq(DepartmentEntity::getHospitalId, hospitalId)
                .like(!StringUtils.isEmpty(departmentEntity.getName()), DepartmentEntity::getName, departmentEntity.getName())
                .page(new Page<>(page, size));
        //2.分页组装
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
