package com.qianxi.wit.manager.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qianxi.wit.manager.pojo.PhysicianRecordEntity;

/**
 * 医生问诊记录表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
public interface PhysicianRecordService extends IService<PhysicianRecordEntity> {

}

