package com.qianxi.wit.manager.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qianxi.common.utils.PageUtils;

import com.qianxi.wit.manager.dao.LiveInfoDao;
import com.qianxi.wit.manager.pojo.LiveInfoEntity;
import com.qianxi.wit.manager.service.LiveInfoService;


@Service("liveInfoService")
public class LiveInfoServiceImpl extends ServiceImpl<LiveInfoDao, LiveInfoEntity> implements LiveInfoService {

}