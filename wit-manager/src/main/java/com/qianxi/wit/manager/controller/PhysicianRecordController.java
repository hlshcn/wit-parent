package com.qianxi.wit.manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qianxi.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.qianxi.wit.manager.pojo.PhysicianRecordEntity;
import com.qianxi.wit.manager.service.PhysicianRecordService;
import com.qianxi.common.utils.PageUtils;


/**
 * 医生问诊记录表
 *
 * @author eight.groups
 * @email wise.infomation.technology@126.com
 * @date 2021-01-07 13:23:32
 */
@RestController
public class PhysicianRecordController {
    @Autowired
    private PhysicianRecordService physicianRecordService;

    /**
     * 5.主治医师诊断记录详情信息API
     * 根据医师ID，分页信息来查询指定医师的诊断记录表，可以模糊查询相关患者在当下医生的诊断信息。
     * http://wise.infomation.com/api/manager/record/physician/{physicianId}/{page}/{size}
     * @param physicianRecordEntity     参数请求体（name模糊查询）
     * @param physicianId               医师ID
     * @param page                      当前页
     * @param size                      每页记录数
     * @return
     */
    @PostMapping("/record/physician/{physicianId}/{page}/{size}")
    public Result findPhysicianRecordByPhysicianId(
        @RequestBody PhysicianRecordEntity physicianRecordEntity,
        @PathVariable("physicianId") Long physicianId,
        @PathVariable("page") int page,
        @PathVariable("size") int size
    ){
        // TODO 1.5 【丁宁，佳圆】 主治医师诊断记录详情信息API
        //1.查询
        IPage<PhysicianRecordEntity> iPage = physicianRecordService.lambdaQuery()
                .eq(PhysicianRecordEntity::getPhysicianId, physicianId)
                .like(!StringUtils.isEmpty(physicianRecordEntity.getPatientName()), PhysicianRecordEntity::getPatientName, physicianRecordEntity.getPatientName())
                .page(new Page<>(page, size));
        //2.数据返回
        PageUtils pageUtils = new PageUtils(iPage.getRecords(), (int)iPage.getTotal(), (int)iPage.getSize(), (int)iPage.getCurrent());
        return Result.ok(pageUtils);
    }
}
