package com.qianxi.wit.vedio.pojo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AudioVideoCallPojo {
    private String roomId = ""; // 发送时候，所在的房间，也是前端选择加入的房间号信息
    private String from = ""; // 发送人
    private String to = ""; // 发送给谁
    private String msg = ""; // 发送的信息
    private String image = ""; // 发送的照片信息(连续播放形成动画)
    private String audio = ""; // 音频信息
    private List<AudioVideoCallPojo> list = new ArrayList<>(); // 包含除了房间号的所有上行刘客户端信息
}
