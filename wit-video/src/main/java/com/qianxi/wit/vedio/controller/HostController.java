package com.qianxi.wit.vedio.controller;

import com.qianxi.wit.vedio.feign.ManagerFeignService;
import com.qianxi.wit.vedio.pojo.TestDemo;
import com.qianxi.wit.vedio.websocket1.AllUserSession;
import com.qianxi.wit.vedio.websocket1.AudioVideoCallServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.Inet4Address;
import java.net.UnknownHostException;

//@CrossOrigin
@RestController
public class HostController {
    @RequestMapping("/host/ip")
    public String getHostIp() throws UnknownHostException {
        String ip = Inet4Address.getLocalHost().getHostAddress();
        System.out.println("服务器IP是：" + ip);
        return ip;
    }

    @RequestMapping("/hello")
    public String hello(){
        System.out.println(managerFeignService);
        System.out.println(redisTemplate);
        System.out.println(testDemo);
        return "hello";
    }

    @Autowired
    private ManagerFeignService managerFeignService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private TestDemo testDemo;
}
