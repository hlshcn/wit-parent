package com.qianxi.wit.vedio.websocket1;

import com.alibaba.fastjson.JSON;
import com.qianxi.wit.vedio.pojo.AudioVideoCallPojo;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@ServerEndpoint("/audio/video/call/server/{roomId}/{username}")
public class AudioVideoCallServer {
    private Session session;
    private String roomId; // 聊天室ID
    private String username; // 当前客户端的姓名

//    private static CopyOnWriteArrayList<AudioVideoCallServer> clients = new CopyOnWriteArrayList<>();
    /**
     * key：存房间号
     * value：存房间号对应的集合，里面包括所在房间里面所有的客户端信息
     */
    public static ConcurrentHashMap<String, CopyOnWriteArrayList<AudioVideoCallServer>> rooms = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(@PathParam("roomId") String roomId, @PathParam("username") String username, Session session) {
        //1.初始化基本信息到本类字段信息
        this.session = session;
        this.roomId = roomId;
        this.username = username;
        //2.判断是否有当前房间号
        CopyOnWriteArrayList<AudioVideoCallServer> clients = rooms.get(roomId);
        //2.1没有当前房间号，直接删除当前客户端
        if (clients == null || clients.size() == 0) {
            clients = new CopyOnWriteArrayList<>();
            clients.add(this);
            rooms.put(roomId, clients);
        } else {
            //2.2有当前房间号，直接添加客户端
            clients.add(this);
        }
    }

    @OnClose
    public void onClose() {
        //1.移除当前房间的所有客户端
        CopyOnWriteArrayList<AudioVideoCallServer> clients = rooms.get(roomId);
        //2.移除当前客户端
        clients.remove(this);
    }

    @OnMessage
    public void onMessage(String message) throws IOException {
        //1.将string类型转换为对象
        AudioVideoCallPojo obj = JSON.parseObject(message, AudioVideoCallPojo.class);

        //2.向当前房间号的除了自己的所有客户端发送消息
        //3.三人  前台传递一个  一个分发两人，
        CopyOnWriteArrayList<AudioVideoCallServer> clients = rooms.get(roomId);
        for (AudioVideoCallServer client : clients) { // 遍历的是每个客户端（包括发送数据的用户一）
            //准备一个新的对象
            AudioVideoCallPojo sendPojo = new AudioVideoCallPojo();
            if (client == this) { // 是自己，不用发送信息，发送另外两个对象的一个空信息
                for (AudioVideoCallServer server : clients) {
                    if (server != this) {
                        sendPojo.getList().add(new AudioVideoCallPojo());
                    }
                }
                //发送，不是自己的两个空数据即可
                client.getSession().getBasicRemote().sendText(JSON.toJSONString(sendPojo));
            } else { // 如果是用户二，发送用户一和用户三的信息给用户二，用户一信息有，用户二信息给空值
                for (AudioVideoCallServer server : clients) {
                    if (server == this) { // 数据来源的类
                        sendPojo.getList().add(obj);
                    } else {
                        if (server != client) { // 保证不发送自己的数据（两路上行流）
                            sendPojo.getList().add(new AudioVideoCallPojo());
                        }
                    }
                }
                // 发送带有一个发送过来的上行刘数据
                client.getSession().getBasicRemote().sendText(JSON.toJSONString(sendPojo));
            }
        }
    }

    public Session getSession() {
        return session;
    }

    @Override
    public String toString() {
        return "AudioVideoCallServer{" +
                "roomId='" + roomId + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
