package com.qianxi.wit.vedio.pojo;

import com.qianxi.wit.vedio.feign.ManagerFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestDemo {
    @Autowired
    private ManagerFeignService managerFeignService;

    public void hello(){
        System.out.println(managerFeignService+"111");
    }
}
