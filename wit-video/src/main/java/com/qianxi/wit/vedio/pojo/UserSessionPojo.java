package com.qianxi.wit.vedio.pojo;

import lombok.Data;

/**
 * 前台接收消息的数据结构
 */
@Data
public class UserSessionPojo {
    private String from; // 发送信息的人
    private String to; // 发送对象
    private String msg; // 要发送的信息
    private String image; // 要发送的图片信息
    private String audio; // 要发送的音频
}
