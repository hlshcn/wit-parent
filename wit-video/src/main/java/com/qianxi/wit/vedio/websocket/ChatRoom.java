package com.qianxi.wit.vedio.websocket;

import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 聊天室
 */
@Component
@ServerEndpoint("/online/chat/room/{username}")
public class ChatRoom {
    //客户端
    private static CopyOnWriteArrayList<ChatRoom> clients = new CopyOnWriteArrayList<>();

    private String username;
    private Session session;

    /**
     * 连接socket
     *
     * @param username
     * @param session
     * @throws UnsupportedEncodingException
     */
    @OnOpen
    public void onOpen(@PathParam("username") String username, Session session) throws UnsupportedEncodingException {
        //0.输出日志信息
        this.username = URLDecoder.decode(username, "UTF-8");
        this.session = session;
        String msg = "[" + getTime() + "]" + this.username + "：加入聊天室...";
        //1.添加客户端
        clients.add(this);
        sendMessage(msg); //发送消息并记录
    }

    /**
     * 关闭socket
     */
    @OnClose
    public void onClose() {
        //0.初始化信息
        String msg = "[" + getTime() + "]" + this.username + "：退出聊天室...";
        //1.退出聊天室
        clients.remove(this);
        sendMessage(msg);//发送消息并记录
    }

    /**
     * 发送消息
     *
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {
        //1.发送消息
        sendMessage("[" + getTime() + "]" + this.username + "：" + message);//发送消息并记录
    }

    private synchronized void sendMessage(String msg) {
        //0.信息
        //1.发送所有的消息
        for (ChatRoom client : clients) {
            //不是本人应用，再开始发送信息
            try {
                client.session.getBasicRemote().sendText(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //2.记录日志
        saveChat(msg);
    }

    /**
     * 保存信息
     *
     * @param msg
     */
    private synchronized void saveChat(String msg) {
        System.out.println(msg);
        try {
            FileWriter writer = new FileWriter("wit-video/src/main/resources/log/chatroom.txt", true);
            writer.write(msg + "\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    private String getTime() {
        synchronized (ChatRoom.class) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        }
    }
}
