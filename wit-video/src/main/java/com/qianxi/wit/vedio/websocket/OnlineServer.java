package com.qianxi.wit.vedio.websocket;

import com.qianxi.wit.vedio.config.SpringBeanUtils;
import com.qianxi.wit.vedio.controller.HostController;
import com.qianxi.wit.vedio.feign.ManagerFeignService;
import com.qianxi.wit.vedio.pojo.TestDemo;
import org.hibernate.validator.internal.constraintvalidators.bv.MaxValidatorForCharSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.server.standard.SpringConfigurator;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 原型，每次连接都会生生成一个OnlineServer对象
 */
@Component
@ServerEndpoint(value = "/online/server/{roomId}")
public class OnlineServer {
    private static StringRedisTemplate redisTemplate;
    private static ManagerFeignService managerFeignService;

    //
    private static CopyOnWriteArrayList<OnlineServer> clients = new CopyOnWriteArrayList<>();
    private static ConcurrentHashMap<String, CopyOnWriteArrayList<OnlineServer>> rooms = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Integer> clientCount = new ConcurrentHashMap<>();

    private Session session;
    private String roomId;

    private static ApplicationContext applicationContext;
    public static void setApplicationContext(ApplicationContext applicationContext) {
        OnlineServer.applicationContext = applicationContext;
        redisTemplate = applicationContext.getBean(StringRedisTemplate.class);
        managerFeignService = applicationContext.getBean(ManagerFeignService.class);
    }

    /**
     * 连接触发的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("roomId") String roomId) {
        System.out.println("一个socket已连接...");
        //1.将当前对象（连接）保存
        this.session = session;
        this.roomId = roomId;
        //2.添加房间信息
        if (rooms.size() == 0 || rooms.get(roomId) == null) {
            CopyOnWriteArrayList<OnlineServer> clients = new CopyOnWriteArrayList<>();
            clients.add(this);
            rooms.put(roomId, clients);
        } else {
            rooms.get(roomId).add(this);
        }
        //3.将当前房间号修改为正在直播
        managerFeignService.updateStateByRoomId("1", roomId);
    }

    /**
     * 关闭触发的方法
     */
    @OnClose
    public void onClose() {
        //移除当前对象（连接）
        rooms.get(roomId).remove(this);
        System.out.println("一个socket已关闭...");
        //3.将当前房间号修改为正在直播
        managerFeignService.updateStateByRoomId("0", roomId);
    }

    /**
     * 服务端接收过来的信息
     *
     * @param message
     * @param session
     */
    @OnMessage
    public void onServerMessage(String message, Session session) {
        //0.从集合拿出信息
        for (OnlineServer client : rooms.get(roomId)) {
            //1.遍历所有客户端
            try {
                //2.发送消息
                client.session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
