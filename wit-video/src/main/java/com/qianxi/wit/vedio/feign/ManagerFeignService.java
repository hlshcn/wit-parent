package com.qianxi.wit.vedio.feign;

import com.qianxi.common.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("wit-manager")
public interface ManagerFeignService {

    @RequestMapping("/manager/update/state/{state}/{roomId}")
    Result updateStateByRoomId(@PathVariable("state") String state, @PathVariable("roomId") String roomId);
}
