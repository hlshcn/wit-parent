package com.qianxi.wit.vedio.websocket1;

import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@ServerEndpoint("/audio/video/call/server/chat/{roomId}/{username}")
public class AudioVideoCallChatServer {
    private Session session;
    private String roomId;
    private String username;

    private static ConcurrentHashMap<String, CopyOnWriteArrayList<AudioVideoCallChatServer>> rooms = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(@PathParam("roomId") String roomId, @PathParam("username") String username, Session session){
        //1.初始化
        this.roomId = roomId;
        this.username = username;
        this.session = session;
        //2.添加
        CopyOnWriteArrayList<AudioVideoCallChatServer> clients = rooms.get(roomId);
        if (clients == null || clients.size() == 0) {
            clients = new CopyOnWriteArrayList<>();
            clients.add(this);
            rooms.put(roomId, clients);
        } else {
            clients.add(this);
        }
    }

    @OnClose
    public void onClose(){
        //1.移除
        CopyOnWriteArrayList<AudioVideoCallChatServer> clients = rooms.get(roomId);
        clients.remove(this);
    }

    @OnMessage
    public void onMessage(String message) throws IOException {
        //1.接收到信息，发送给客户端的搜有人
        CopyOnWriteArrayList<AudioVideoCallChatServer> clients = rooms.get(roomId);
        for (AudioVideoCallChatServer client : clients) {
            if (client != this) {
                client.session.getBasicRemote().sendText(message);
            }
        }
    }
}
