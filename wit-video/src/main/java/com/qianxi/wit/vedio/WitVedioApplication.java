package com.qianxi.wit.vedio;

import com.qianxi.wit.vedio.websocket.OnlineServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class WitVedioApplication {

    public static void main(String[] args) {
//        SpringApplication.run(WitVedioApplication.class, args);

        SpringApplication springApplication = new SpringApplication(WitVedioApplication.class);
        ConfigurableApplicationContext context = springApplication.run(args);
        OnlineServer.setApplicationContext(context);
    }

}
