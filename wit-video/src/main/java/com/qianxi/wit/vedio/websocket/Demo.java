package com.qianxi.wit.vedio.websocket;

import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

@Component
@ServerEndpoint("/users/server/session/username")
public class Demo {
    @OnOpen
    public void onOpen(){

    }

    @OnClose
    public void onClose(){

    }

    @OnMessage
    public void onMessage(String message){

    }
}
