package com.qianxi.wit.vedio.websocket1;

import com.alibaba.fastjson.JSON;
import com.qianxi.wit.vedio.pojo.UserSessionPojo;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 存放所有的用户上线的信息
 */
@Component
@ServerEndpoint("/users/server/session/{username}")
public class AllUserSession {
    private Session session;
    private String username;
    public static ConcurrentHashMap<String, AllUserSession> clients = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username){
        //1.初始化信息
        this.session = session;
        this.username = username;
        //2.控制台输出
        System.out.println(this.username+"上线了...");
        clients.put(this.username, this);
    }

    @OnClose
    public void onClose(){
        //1.移除信息
        clients.remove(username);
        System.out.println(username+"下线了...");
    }

    /**
     * 单人聊天
     * map
     *  to
     * @param message
     */
    @OnMessage
    public void onMessage(String message) throws IOException {
        //1.接收到的消息是
        UserSessionPojo obj = JSON.parseObject(message, UserSessionPojo.class);
//        System.out.println(obj);
        //2.向指定的人发送消息
        for (Map.Entry<String, AllUserSession> entry : clients.entrySet()) {
            if (entry.getKey().equals(obj.getTo())) { // 匹配发送人
                entry.getValue().getSession().getBasicRemote().sendText(message); // 发送消息
                System.out.println(this.username+"给"+entry.getValue().getUsername()+"发送的消息是："+message);
            }
        }
    }

    @OnError
    public void onError(Throwable throwable){
        throwable.printStackTrace();
        clients.remove(this);
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "AllUserSession{" +
                "session=" + session +
                ", username='" + username + '\'' +
                '}';
    }
}
